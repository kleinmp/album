<?php

namespace Drupal\album\Cache;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Defines the artist node cache context service.
 *
 * Cache context ID: 'route.artist_node'.
 *
 * This allows for artist node location-aware caching. It depends on:
 * - whether the current route represents an artist node
 * - or whether the current route represents an album node related to an artist
 */
class ArtistNodeCacheContext implements CacheContextInterface, ContainerAwareInterface {

  use ContainerAwareTrait;

  /**
   * The currently active request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected Request $request;

  /**
   * Constructs a new ArtistNodeCacheContext service.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(RequestStack $request_stack) {
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t("Artist Node");
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    $artist_id = $this->getArtistNodeId();
    $node = $this->request->get('node');

    if (empty($node) || empty($artist_id)) {
      return 'artist_node.none';
    }

    return 'artist_node.' . $node->id();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    // The book active trail depends on the node and data attached to it.
    // That information is however not stored as part of the node.
    $cacheable_metadata = new CacheableMetadata();
    if ($artist_id = $this->getArtistNodeId()) {
      $cacheable_metadata->addCacheTags(['node:' . $artist_id]);
    }
    return $cacheable_metadata;
  }

  /**
   * Get the artist node id context.
   *
   * @return int|null
   *   The id or NULL if it doesn't exist.
   */
  protected function getArtistNodeId() : int|null {
    $artist_id = NULL;
    if ($node = $this->request->get('node')) {
      switch ($node->bundle()) {
        case 'album':
          if ($node->field_artist_to_album->entity) {
            $artist_id = $node->field_artist_to_album->entity->id();
          }
          break;

        case 'artist':
          $artist_id = $node->id();
          break;
      }
    }
    return $artist_id;
  }

}
