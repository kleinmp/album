<?php

namespace Drupal\album;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\node\NodeInterface;

/**
 * Album service.
 */
class AlbumApi {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * AlbumApi constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger channel.
   */
  public function __construct(
    Connection $database,
    EntityTypeManagerInterface $entity_type_manager,
    LoggerChannelInterface $logger,
  ) {
    $this->database = $database;
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
  }

  /**
   * Returns the entity query object for this entity type.
   *
   * @param string $entity_type
   *   The entity type (for example, node) for which the query object should be
   *   returned.
   * @param string $conjunction
   *   (optional) Either 'AND' if all conditions in the query need to apply, or
   *   'OR' if any of them is sufficient. Defaults to 'AND'.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   The query object that can query the given entity type.
   */
  public function entityQuery(string $entity_type, string $conjunction = 'AND') : QueryInterface {
    return $this->entityTypeManager->getStorage($entity_type)->getQuery($conjunction);
  }

  /**
   * Compute the length of an album.
   *
   * @param \Drupal\node\NodeInterface $album
   *   The album node.
   */
  public function computeAlbumLength(NodeInterface $album) : void {
    $length = 0;
    if (!empty($album->field_tracks)) {
      foreach ($album->field_tracks->referencedEntities() as $track) {
        $length += $track->get('field_length')->value;
      }
    }
    $album->field_album_length->value = $length;
  }

  /**
   * Renumber the tracks on an album so they stay in order.
   *
   * @param \Drupal\node\NodeInterface $album
   *   The album node.
   */
  public function renumberTracks(NodeInterface $album) : void {
    if (!empty($album->field_tracks)) {
      foreach ($album->field_tracks->referencedEntities() as $delta => $track) {
        $number = $track->get('field_track_number')->value;
        if (empty($number) || $number != ($delta + 1)) {
          $track->field_track_number->value = $delta + 1;
          $track->save();
        }
      }
    }
  }

  /**
   * Compute the values on artist nodes.
   *
   * @param \Drupal\node\NodeInterface $artist
   *   The artist node.
   */
  public function computeArtistValues(NodeInterface $artist) : void {
    $total_length = 0;
    $total_years = 0;
    $total_tracks = 0;
    $album_ids = $this->getAlbums([
      'artist_id' => $artist->id(),
    ]);

    $album_count = count($album_ids);

    foreach ($album_ids as $album_id) {
      $album = $this->entityTypeManager->getStorage('node')->load($album_id);
      $total_length += $album->get('field_album_length')->value;
      $total_years += $album->get('field_year')->value;
      $total_tracks += count($album->get('field_tracks')->referencedEntities());
    }

    $artist->set('field_artist_album_count', $album_count);
    $artist->set('field_artist_total_length', $total_length);
    $artist->set('field_artist_track_count', $total_tracks);
    $artist->set('field_artist_avg_length', $total_length / $album_count);
    $artist->set('field_artist_avg_song', $total_tracks / $album_count);
    $artist->set('field_artist_avg_year', $total_years / $album_count);
  }

  /**
   * Check if a node exists with the given title.
   *
   * @param string $title
   *   The title to search for.
   * @param string $type
   *   Restrict the results by node type.
   *
   * @return array
   *   An array of node ids.
   */
  public function contentExists(string $title, string $type) :array {
    return $this->entityQuery('node')
      ->condition('type', $type)
      ->condition('title', $title)
      ->execute();
  }

  /**
   * Get a list of albums based.
   *
   * @param array $options
   *   (optional) Options for sorting and filtering the results.
   *   See below for details.
   *
   * @return array
   *   An array of node ids or \Drupal\node\NodeInterface if loaded.
   */
  public function getAlbums(array $options = []) : array {
    $options += [
      // The field for ordering the results.
      // This must be one of the allowed fields
      // or a column on the node table
      // or 'rand' for random order.
      'order' => 'n.nid',
      // The direction of the order.
      'order_direction' => 'ASC',
      // Filter the results by albums
      // related to an artist.
      'artist_id' => NULL,
      // Limit the results with a year range.
      'years' => [],
      // Don't return these album ids.
      'skip_albums' => [],
      // The total number of results to return.
      // Empty will return all.
      'count' => NULL,
      // Whether or not to load the nodes.
      'load' => FALSE,
    ];

    // Make sure results are filtered.  We can't load everything.
    if (empty($options['artist_id']) && empty($options['years']) && !isset($options['count']) && !empty($options['load'])) {
      return [];
    }

    $albums = [];
    $allowed_order_fields = [
      'field_received',
      'field_album_length',
      'field_year',
    ];
    $query = $this->database->select('node', 'n');
    $query->innerJoin('node_field_data', 'nfd', 'n.vid = nfd.vid');
    if (!empty($options['artist_id'])) {
      $query->innerJoin('node__field_artist_to_album', 'ar', 'n.vid = ar.revision_id');
      $query->condition('ar.field_artist_to_album_target_id', $options['artist_id']);
    }
    if (!empty($options['skip_albums'])) {
      $query->condition('n.nid', $options['skip_albums'], 'NOT IN');
    }
    $query->fields('n', ['nid'])
      ->condition('n.type', 'album')
      ->condition('nfd.status', 1);
    if ($options['order'] == 'rand') {
      $query->addExpression('RAND()', 'rand');
    }
    elseif (in_array($options['order'], $allowed_order_fields)) {
      $query->innerJoin('node__' . $options['order'], $options['order'], 'n.vid = ' . $options['order'] . '.revision_id');
      $options['order'] .= '_value';
    }
    if (!empty($options['years'])) {
      $query->innerJoin('node__field_year', 'year', 'n.vid = year.revision_id');
      $query->condition('year.field_year_value', $options['years']['start'], '>=');
      $query->condition('year.field_year_value', $options['years']['end'], '<=');
    }

    $query->orderBy($options['order'], $options['order_direction']);
    if (!empty($options['count'])) {
      $query->range(0, $options['count']);
    }
    $results = $query->execute();
    while ($nid = $results->fetchField()) {
      $albums[$nid] = $options['load'] ? $this->entityTypeManager->getStorage('node')->load($nid) : $nid;
    }
    return $albums;
  }

  /**
   * Find best album nodes.
   *
   * @param string $field
   *   The field to search
   *   (field_best_year, field_best_decade, field_best_all_time).
   * @param array $options
   *   An array of options. See below.
   *
   * @return array
   *   An array of Drupal\node\NodeInterface.
   */
  public function getBestAlbums(string $field, array $options = []) : array {
    $nodes = [];
    $options += [
      // Limit the results with a year range.
      'years' => [],
      // Get albums that are not best of.
      'negate' => FALSE,
      // Add other filters.
      'conditions' => [],
      // Order the results by this field.
      'order' => $field,
    ];
    $query = $this->entityQuery('node')
      ->condition('type', 'album');
    if (!$options['negate']) {
      $query->condition($field . '.value', 0, '>');
    }
    else {
      $query->condition($field . '.value', NULL, 'IS NULL');
    }

    if (!empty($options['years'])) {
      $query->condition('field_year.value', $options['years']['start'], '>=');
      $query->condition('field_year.value', $options['years']['end'], '<=');
    }

    if (!empty($options['conditions'])) {
      foreach ($options['conditions'] as $condition) {
        $query->condition($condition['field'], $condition['value'], $condition['operator']);
      }
    }

    if ($nids = $query->execute()) {
      $storage_handler = $this->entityTypeManager->getStorage('node');
      $nodes = $storage_handler->loadMultiple($nids);
      if ($options['order']) {
        $order_field = $options['order'];
        uasort($nodes, function ($a, $b) use ($order_field) {
          if (!$a->get($order_field)->value) {
            if (!$b->get($order_field)->value) {
              return 0;
            }
            return 1;
          }
          if (!$b->get($order_field)->value) {
            return -1;
          }
          return $a->get($order_field)->value <=> $b->get($order_field)->value;
        });
      }
    }
    return $nodes;
  }

  /**
   * Get bulk album statistics.
   *
   * @param array $years
   *   (optional) A date range to filter totals.
   * @param array $dates_received
   *   (optional) Limit results by dates albums were received.
   *
   * @return array
   *   An array of bulk results.
   */
  public function getAlbumTotals(array $years = [], array $dates_received = []) : array {
    $fields = [
      'field_received',
      'field_year',
      'field_track_count',
      'field_album_length',
      'field_best_year',
      'field_best_decade',
      'field_best_all_time',
    ];
    $query = $this->database->select('node', 'n');
    foreach ($fields as $field) {
      $query->leftJoin('node__' . $field, $field, 'n.vid = ' . $field . '.revision_id');
    }
    $query->condition('n.type', 'album');
    if (!empty($years)) {
      $query->condition('field_year.field_year_value', $years['start'], '>=');
      $query->condition('field_year.field_year_value', $years['end'], '<=');
    }
    foreach (['start' => '>=', 'end' => '<='] as $date_key => $operator) {
      if (!empty($dates_received[$date_key])) {
        $query->condition('field_received.field_received_value', $dates_received[$date_key], $operator);
      }
    }
    $query->addExpression('SUM(field_year.field_year_value)', 'sum_year');
    $query->addExpression('SUM(field_album_length.field_album_length_value)', 'sum_length');
    $query->addExpression('SUM(field_track_count.field_track_count_value)', 'sum_tracks');
    $query->addExpression('COUNT(field_best_year.field_best_year_value)', 'count_best_year');
    $query->addExpression('COUNT(field_best_decade.field_best_decade_value)', 'count_best_decade');
    $query->addExpression('COUNT(field_best_all_time.field_best_all_time_value)', 'count_best_all_time');
    $query->addExpression('AVG(field_year.field_year_value)', 'avg_year');
    $query->addExpression('AVG(field_album_length.field_album_length_value)', 'avg_length');
    $query->addExpression('AVG(field_track_count.field_track_count_value)', 'avg_tracks');
    $query->addExpression('COUNT(n.nid)', 'count_albums');
    return $query->execute()->fetchAssoc();
  }

  /**
   * Get album scores for each artist.
   *
   * @param string|null $start
   *   (optional) Only count albums during or after this year.
   * @param string|null $end
   *   (optional) Only count albums during or prior to this year.
   *
   * @return array
   *   The scores for each album during the given years.
   */
  public function getArtistScores(?string $start = NULL, ?string $end = NULL) : array {
    $album_scores = $this->getAlbumScores($start, $end);
    $track_scores = $this->getTrackScores($start, $end);
    $titles = $this->getArtistTitles();
    $collabs = $this->getArtistCollaborations();

    $artists = [];
    $fields = [
      'count_albums',
      'count_best_year',
      'count_best_decade',
      'count_best_all_time',
    ];
    if (!empty($album_scores)) {
      foreach ($album_scores as $nid => $score) {
        $artist = (array) clone $score;
        $artist['count_best_tracks'] = !empty($track_scores[$nid]->count_best_tracks) ? $track_scores[$nid]->count_best_tracks : 0;
        $artist['title'] = $titles[$nid]->title;
        foreach ($collabs as $collaboration) {
          if ($collaboration->nid == $nid && isset($album_scores[$collaboration->field_artist_collaborations_target_id])) {
            foreach ($fields as $field) {
              $artist[$field] += $album_scores[$collaboration->field_artist_collaborations_target_id]->{$field};
            }
            if (!empty($track_scores[$collaboration->field_artist_collaborations_target_id]->count_best_tracks)) {
              $artist['count_best_tracks'] += $track_scores[$collaboration->field_artist_collaborations_target_id]->count_best_tracks;
            }
          }
          $artist['score'] = $this->computeScore($artist);
          $artists[$nid] = $artist;
        }
      }
    }

    uasort($artists, function ($a, $b) {
      foreach ([
        'score',
        'count_best_all_time',
        'count_best_decade',
        'count_best_year',
        'count_best_tracks',
        'count_albums',
        'title',
      ] as $key) {
        if ($a[$key] != $b[$key]) {
          return $a[$key] > $b[$key] ? -1 : 1;
        }
      }
      return 0;
    });
    return $artists;
  }

  /**
   * Compute the score for an individual album.
   *
   * @param \Drupal\node\NodeInterface $album
   *   An album node.
   *
   * @return float
   *   The computed score.
   */
  public function computeAlbumScore(NodeInterface $album) : float {
    $values = [
      'count_best_tracks' => $album->get('best_track')->value,
      'count_albums' => 1,
      'count_best_year' => (bool) $album->get('field_best_year')->value,
      'count_best_decade' => (bool) $album->get('field_best_decade')->value,
      'count_best_all_time' => (bool) $album->get('field_best_all_time')->value,
    ];
    return $this->computeScore($values);
  }

  /**
   * Compute the score for an individual artist.
   *
   * @param Drupal\node\NodeInterface $artist
   *   An artist node.
   * @param array $values
   *   (Optional) Initial values of the albums count.
   *
   * @return float
   *   The computed score.
   */
  public function computeArtistScore(NodeInterface $artist, array &$values = []) : float {
    $values += [
      'count_best_tracks' => 0,
      'count_albums' => 0,
      'count_best_year' => 0,
      'count_best_decade' => 0,
      'count_best_all_time' => 0,
    ];
    $album_ids = [];
    if ($artist->id()) {
      $album_ids = $this->getAlbums([
        'artist_id' => $artist->id(),
      ]);
    }

    $values['count_albums'] += count($album_ids);

    $albums = $this->entityTypeManager->getStorage('node')->loadMultiple($album_ids);
    foreach ($albums as $album) {
      if ($album->get('best_track')->value) {
        $values['count_best_tracks']++;
      }
      if ($album->get('field_best_year')->value) {
        $values['count_best_year']++;
      }
      if ($album->get('field_best_decade')->value) {
        $values['count_best_decade']++;
      }
      if ($album->get('field_best_all_time')->value) {
        $values['count_best_all_time']++;
      }
    }

    // Add values for collaborations.
    // This method is recursive.
    $collaborations = $artist->field_artist_collaborations->referencedEntities();
    if (!empty($collaborations)) {
      foreach ($collaborations as $collaboration) {
        $this->computeArtistScore($collaboration, $values);
      }
    }

    return $this->computeScore($values);
  }

  /**
   * Compute the score for the given values.
   *
   * @param array $values
   *   An array containing album count information for an artist.
   *
   * @return float
   *   The computed score.
   */
  public function computeScore(array $values) : float {
    $values += [
      'count_best_tracks' => 0,
      'count_albums' => 0,
      'count_best_year' => 0,
      'count_best_decade' => 0,
      'count_best_all_time' => 0,
    ];
    return 0.25 * $values['count_best_tracks'] +
      $values['count_albums'] +
      $values['count_best_year'] +
      2 * $values['count_best_decade'] +
      4 * $values['count_best_all_time'];
  }

  /**
   * Query for album information to get scores.
   *
   * @param string|null $start
   *   (optional) Only count albums during or after this year.
   * @param string|null $end
   *   (optional) Only count albums during or prior to this year.
   *
   * @return array
   *   An array of album counts keyed by artist id.
   */
  public function getAlbumScores(?string $start = NULL, ?string $end = NULL) : array {
    $fields = [
      'field_year',
      'field_artist_to_album',
      'field_best_year',
      'field_best_decade',
      'field_best_all_time',
    ];
    $query = $this->database->select('node', 'n');
    foreach ($fields as $field) {
      $query->leftJoin('node__' . $field, $field, 'n.vid = ' . $field . '.revision_id');
    }
    $query->leftJoin('node_field_data', 'artist', 'artist.nid = field_artist_to_album.field_artist_to_album_target_id');
    $query->condition('n.type', 'album');
    if (!empty($start)) {
      $query->condition('field_year.field_year_value', $start, '>=');
    }
    if (!empty($end)) {
      $query->condition('field_year.field_year_value', $end, '<=');
    }
    $query->groupBy('artist.nid');

    $query->fields('artist', ['nid']);
    $query->addExpression('COUNT(field_best_year.field_best_year_value)', 'count_best_year');
    $query->addExpression('COUNT(field_best_decade.field_best_decade_value)', 'count_best_decade');
    $query->addExpression('COUNT(field_best_all_time.field_best_all_time_value)', 'count_best_all_time');
    $query->addExpression('COUNT(n.nid)', 'count_albums');
    return $query->execute()->fetchAllAssoc('nid');
  }

  /**
   * Query for track information to get scores.
   *
   * @param string|null $start
   *   (optional) Only count tracks during or after this year.
   * @param string|null $end
   *   (optional) Only count tracks during or prior to this year.
   *
   * @return array
   *   An array of track counts keyed by artist id.
   */
  public function getTrackScores(?string $start = NULL, ?string $end = NULL) : array {
    $fields = [
      'field_year',
      'field_tracks',
      'field_artist_to_album',
    ];
    $query = $this->database->select('node', 'n');
    foreach ($fields as $field) {
      $query->leftJoin('node__' . $field, $field, 'n.vid = ' . $field . '.revision_id');
    }
    $query->leftJoin('paragraph__field_best_of_year', 'best', 'field_tracks.field_tracks_target_revision_id = best.revision_id');

    $query->leftJoin('node_field_data', 'artist', 'artist.nid = field_artist_to_album.field_artist_to_album_target_id');
    $query->condition('n.type', 'album');
    $query->condition('best.field_best_of_year_value', 1);

    if (!empty($start)) {
      $query->condition('field_year.field_year_value', $start, '>=');
    }
    if (!empty($end)) {
      $query->condition('field_year.field_year_value', $end, '<=');
    }
    $query->groupBy('artist.nid');
    $query->fields('artist', ['nid']);
    $query->addExpression('COUNT(best.field_best_of_year_value)', 'count_best_tracks');
    return $query->execute()->fetchAllAssoc('nid');
  }

  /**
   * Get all artist titles.
   *
   * @return array
   *   An array of artist titles keyed by id.
   */
  public function getArtistTitles() : array {
    $query = $this->database->select('node', 'n');
    $query->leftJoin('node_field_data', 'fd', 'n.vid = fd.vid');
    $query->condition('n.type', 'artist');

    $query->fields('n', ['nid']);
    $query->fields('fd', ['title']);
    return $query->execute()->fetchAllAssoc('nid');
  }

  /**
   * Get all collaborations on artists.
   *
   * @return array
   *   An array mapping artist ids to collaborator artist ids.
   */
  public function getArtistCollaborations() : array {
    $query = $this->database->select('node', 'n');
    $query->leftJoin('node__field_artist_collaborations', 'field_artist_collaborations', 'n.vid = field_artist_collaborations.revision_id');
    $query->leftJoin('node_field_data', 'fd', 'n.vid = fd.vid');
    $query->condition('n.type', 'artist');
    $query->condition('field_artist_collaborations.field_artist_collaborations_target_id', NULL, 'IS NOT NULL');

    $query->fields('n', ['nid']);
    $query->fields('field_artist_collaborations', ['field_artist_collaborations_target_id']);
    return $query->execute()->fetchAll();
  }

  /**
   * Format a time duration into a human readable time.
   *
   * @param int $milliseconds
   *   The number of milliseconds to be converted.
   *
   * @return string
   *   The converted time to Hours:Minutes:Seconds.
   */
  public function formatTime(int $milliseconds) : string {
    $total_seconds = (int) (($milliseconds - 100) * 0.001);
    $total_minutes = floor($total_seconds / 60);
    $hours = floor($total_minutes / 60);
    $minutes = str_pad($total_minutes % 60, 2, 0, STR_PAD_LEFT);
    $seconds = str_pad($total_seconds % 60, 2, 0, STR_PAD_LEFT);

    if ($hours) {
      return ltrim("$hours:$minutes:$seconds", 0);
    }
    elseif (intval($minutes)) {
      return ltrim("$minutes:$seconds", 0);
    }
    return "0:$seconds";
  }

  /**
   * Get maximum 'best of' album count for a period of time.
   *
   * @param string $type
   *   The type of 'best of' count to calculate
   *   See the allowed types below.
   * @param int $year
   *   (optional) The first year to look at given the type.
   *   This is not valid for type 'field_best_all_time'.
   *
   * @return int
   *   The number of maximum allowed 'best of' albums.
   */
  public function getMaxBestOfCount(string $type, int $year = 0) : int {
    $types = [
      'field_best_year' => [
        'base' => 5,
        'total_base' => 15,
        'divisor' => 5,
        'query_options' => [
          'years' => [
            'start' => $year,
            'end' => $year,
          ],
        ],
      ],
      'field_best_decade' => [
        'base' => 20,
        'total_base' => 100,
        'divisor' => 10,
        'query_options' => [
          'years' => [
            'start' => $year,
            'end' => $year + 9,
          ],
        ],
      ],
      'field_best_all_time' => [
        'base' => 50,
        'total_base' => 500,
        'divisor' => 20,
        'query_options' => [],
      ],
    ];
    $current = $types[$type];
    $album_ids = $this->getAlbums($current['query_options']);
    $total = count($album_ids);

    return (int) ($total <= $current['total_base']) ? $current['base'] : $current['base'] + floor(($total - $current['total_base']) / $current['divisor']);
  }

}
