<?php

namespace Drupal\album\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller routines best album manager.
 */
class BestAlbumsManager extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  protected function getModuleName() {
    return 'album';
  }

  /**
   * Page for displaying best albums of a year.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   */
  public function bestYearPage(int $year) {
    if (!is_int($year) || !ctype_digit((string) $year)) {
      throw new NotFoundHttpException();
    }
    if ($year < 1900 || $year > date('Y') + 1) {
      throw new NotFoundHttpException();
    }

    $build = [];
    $build['best_year_link'] = [
      '#title' => $this->t('View Year List'),
      '#type' => 'link',
      '#url' => Url::fromRoute('view.albums.albums_page', [], [
        'query' => [
          'order' => 'field_best_year_int',
          'sort' => 'asc',
          'f' => [
            "year:$year",
            'best_of_year_node:1',
          ],
        ],
      ]),
    ];
    $build['best_album_add'] = $this->formBuilder()->getForm('Drupal\album\Form\BestAlbumAdd', 'field_best_year', [
      'years' => [
        'start' => $year,
        'end' => $year,
      ],
      'order' => 'title',
    ]);
    $build['best_album_order'] = $this->formBuilder()->getForm('Drupal\album\Form\BestAlbumOrder', 'field_best_year', [
      'years' => [
        'start' => $year,
        'end' => $year,
      ],
    ]);
    return $build;
  }

  /**
   * Page for displaying best albums of a decade.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   */
  public function bestDecadePage(int $decade) {
    if (!is_int($decade) || !ctype_digit($decade)) {
      throw new NotFoundHttpException();
    }
    if ($decade < 1900 || $decade > date('Y') + 1) {
      throw new NotFoundHttpException();
    }
    if ($decade % 10 !== 0) {
      throw new NotFoundHttpException();
    }

    $build = [];
    $build['best_decade_link'] = [
      '#title' => $this->t('View Albums List'),
      '#type' => 'link',
      '#url' => Url::fromRoute('view.albums.albums_page', [], [
        'query' => [
          'start' => $decade . '-01-01',
          'end' => $decade + 9 . '-12-31',
          'order' => 'field_best_decade_int',
          'sort' => 'asc',
          'f' => [
            'best_of_decade:1',
          ],
        ],
      ]),
    ];
    $build['best_album_add'] = $this->formBuilder()->getForm('Drupal\album\Form\BestAlbumAdd', 'field_best_decade', [
      'years' => [
        'start' => $decade,
        'end' => $decade + 9,
      ],
      'order' => 'field_best_year',
    ]);
    $build['best_album_order'] = $this->formBuilder()->getForm('Drupal\album\Form\BestAlbumOrder', 'field_best_decade', [
      'years' => [
        'start' => $decade,
        'end' => $decade + 9,
      ],
    ]);
    return $build;
  }

  /**
   * Page for displaying best albums all time.
   */
  public function bestAllTimePage() {
    $build = [];
    $build['best_link'] = [
      '#title' => $this->t('View All Time List'),
      '#type' => 'link',
      '#url' => Url::fromRoute('view.albums.albums_page', [], [
        'query' => [
          'order' => 'field_best_all_time_int',
          'sort' => 'asc',
          'f' => [
            'best_of_all_time:1',
          ],
        ],
      ]),
    ];
    $build['best_album_add'] = $this->formBuilder()->getForm('Drupal\album\Form\BestAlbumAdd', 'field_best_all_time', [
      'order' => 'field_best_decade',
      'conditions' => [
        [
          'field' => 'field_best_decade.value',
          'value' => NULL,
          'operator' => 'IS NOT NULL',
        ],
      ],
    ]);
    $build['best_album_order'] = $this->formBuilder()->getForm('Drupal\album\Form\BestAlbumOrder', 'field_best_all_time');
    return $build;
  }

  /**
   * Get the title of a best of year page.
   */
  public function bestYearTitle($year) {
    return 'Best of Year Manager (' . $year . ')';
  }

  /**
   * Get the title of a best of decade page.
   */
  public function bestDecadeTitle($decade) {
    return 'Best of Decade Manager (' . $decade . ')';
  }

}
