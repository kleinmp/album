<?php

namespace Drupal\album\Controller;

use Drupal\album\AlbumApi;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller for totals pages.
 */
class Totals extends ControllerBase {

  /**
   * The album api.
   *
   * @var \Drupal\album\AlbumApi
   */
  protected AlbumApi $albumApi;

  /**
   * The currently active request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected Request $request;

  /**
   * AlbumImportApi constructor.
   *
   * @param \Drupal\album\AlbumApi $album_api
   *   The album api.
   * @param Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(AlbumApi $album_api, RequestStack $request_stack) {
    $this->albumApi = $album_api;
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Note: https://github.com/mglaman/drupal-check/issues/136
    return new static(
      $container->get('album.album_api'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getModuleName() {
    return 'album';
  }

  /**
   * Page for displaying album totals by release year.
   */
  public function totalsByReleaseYear() {
    $date_filters = $this->getDateFilters();
    $totals = [
      'All' => [
        'Total' => $this->albumApi->getAlbumTotals([], $date_filters),
      ],
      'Decades' => [],
      'Years' => [],
    ];
    foreach ($this->getDecades() as $decade => $years) {
      $totals['Decades'][$decade] = $this->albumApi->getAlbumTotals($years, $date_filters);
    }
    foreach (range(1950, date('Y')) as $year) {
      $totals['Years'][$year] = $this->albumApi->getAlbumTotals(['start' => $year, 'end' => $year], $date_filters);
    }
    $year_count = count($totals['Years']);
    $totals['All']['Average'] = [
      'count_albums' => number_format($totals['All']['Total']['count_albums'] / $year_count, 2),
      'sum_length' => $totals['All']['Total']['sum_length'] / $year_count,
      'sum_tracks' => number_format($totals['All']['Total']['sum_tracks'] / $year_count, 2),
      'count_best_year' => number_format($totals['All']['Total']['count_best_year'] / $year_count, 2),
      'count_best_decade' => number_format($totals['All']['Total']['count_best_decade'] / $year_count, 2),
      'count_best_all_time' => number_format($totals['All']['Total']['count_best_all_time'] / $year_count, 2),
    ];
    return $this->buildTotalsTable($totals, $date_filters);
  }

  /**
   * Page for displaying album totals by received date.
   */
  public function totalsByReceiveDate() {
    $date_filters = $this->getDateFilters();
    $date_filters['start'] = (!empty($date_filters['start']) && $date_filters['start'] > '1996-12-25') ? $date_filters['start'] : '1996-12-25';
    $date_filters['end'] = (!empty($date_filters['end']) && $date_filters['end'] < date('Y-m-d')) ? $date_filters['end'] : date('Y-m-d');

    $totals = [
      'All' => [
        'Total' => $this->albumApi->getAlbumTotals([], $date_filters),
      ],
      'Decades' => [],
      'Years' => [],
    ];
    foreach ($this->getDecades(1990) as $decade => $years) {
      $years['start'] = $years['start'] . '-01-01';
      $years['end'] = $years['end'] . '-12-31';

      $years['start'] = $years['start'] < $date_filters['start'] ? $date_filters['start'] : $years['start'];
      $years['end'] = $years['end'] > $date_filters['end'] ? $date_filters['end'] : $years['end'];
      $totals['Decades'][$decade] = $this->albumApi->getAlbumTotals([], $years);
    }
    foreach (range(1996, date('Y')) as $year) {
      $years = [
        'start' => $year . '-01-01',
        'end' => $year . '-12-31',
      ];

      $years['start'] = $years['start'] < $date_filters['start'] ? $date_filters['start'] : $years['start'];
      $years['end'] = $years['end'] > $date_filters['end'] ? $date_filters['end'] : $years['end'];

      $totals['Years'][$year] = $this->albumApi->getAlbumTotals([], $years);
    }
    $year_count = count($totals['Years']);
    $totals['All']['Average'] = [
      'count_albums' => number_format($totals['All']['Total']['count_albums'] / $year_count, 2),
      'sum_length' => $totals['All']['Total']['sum_length'] / $year_count,
      'sum_tracks' => number_format($totals['All']['Total']['sum_tracks'] / $year_count, 2),
      'count_best_year' => number_format($totals['All']['Total']['count_best_year'] / $year_count, 2),
      'count_best_decade' => number_format($totals['All']['Total']['count_best_decade'] / $year_count, 2),
      'count_best_all_time' => number_format($totals['All']['Total']['count_best_all_time'] / $year_count, 2),
    ];
    return $this->buildTotalsTable($totals, $date_filters);
  }

  /**
   * Page for displaying artist scores.
   */
  public function artistTotalsPage() {
    $date_filters = $this->getDateFilters('Y');
    foreach (['start', 'end'] as $date_key) {
      if (empty($date_filters[$date_key])) {
        $date_filters[$date_key] = NULL;
      }
    }
    $scores = $this->albumApi->getArtistScores($date_filters['start'], $date_filters['end']);

    $build = [];
    $build['#cache']['tags'] = ['node_type:album'];
    $build['#cache']['contexts'] = [
      'url.path',
      'url.query_args:start',
      'url.query_args:end',
    ];

    $build['table'] = [
      '#type' => 'table',
      '#header' => [
        '',
        '',
        $this->t('Albums'),
        $this->t('Best of Year'),
        $this->t('Best of Decade'),
        $this->t('Best of All'),
        $this->t('Best Tracks'),
        $this->t('Score'),
      ],
      '#empty' => $this->t('There are no artists yet.'),
    ];

    if (!empty($scores)) {
      $rank = $count = 0;
      $last_score = NULL;
      foreach ($scores as $nid => $score) {
        $count++;
        $build['table'][$nid] = [];
        if (!isset($last_score) || $last_score > $score['score']) {
          $last_score = $score['score'];
          $rank = $count;
        }
        $build['table'][$nid]['rank'] = [
          '#markup' => $rank . '.',
        ];
        $build['table'][$nid]['title'] = [
          '#title' => $this->t($score['title']),
          '#type' => 'link',
          '#url' => Url::fromRoute('entity.node.canonical', ['node' => $nid], []),
        ];
        foreach ([
          'count_albums',
          'count_best_year',
          'count_best_decade',
          'count_best_all_time',
          'count_best_tracks',
          'score',
        ] as $key) {
          $build['table'][$nid][$key] = [
            '#markup' => isset($score[$key]) ? Xss::filter($score[$key]) : '-',
          ];
        }
      }
    }
    return $build;
  }

  /**
   * Build a render array for displaying album totals.
   *
   * @param array $totals
   *   The album totals in the format given by AlbumApi::getAlbumTotals.
   * @param array $date_filters
   *   (optional) Array with start and end dates for the date filter form.
   *
   * @return array
   *   The rendered totals table.
   */
  public function buildTotalsTable(array $totals, array $date_filters = []) {
    $date_filters = empty($date_filters) ? $this->getDateFilters() : $date_filters;
    $build['#cache']['tags'] = ['node_type:album'];
    $build['#cache']['contexts'] = [
      'url.path',
      'url.query_args:start',
      'url.query_args:end',
    ];

    foreach ($totals as $key => $data) {
      $build[$key] = [
        '#markup' => '<h2>' . $this->t('@key Totals', ['@key' => $key]) . '</h2>',
      ];
      $build[$key]['table'] = [
        '#type' => 'table',
        '#header' => [
          '',
          $this->t('Albums'),
          $this->t('Avg Year'),
          $this->t('Total Length'),
          $this->t('Avg Length'),
          $this->t('Total Tracks'),
          $this->t('Avg Track'),
          $this->t('Best of Year'),
          $this->t('Best of Decade'),
          $this->t('Best of All Time'),
        ],
        '#empty' => $this->t('There are no albums yet.'),
      ];

      if (!empty($data)) {
        foreach ($data as $row => $results) {
          $row = (string) $row;
          $build[$key]['table'][$row] = [];
          $build[$key]['table'][$row]['type'] = [
            '#markup' => $this->t($row),
          ];
          $build[$key]['table'][$row]['albums'] = [
            '#markup' => !empty($results['count_albums']) ? $results['count_albums'] : '-',
          ];
          $build[$key]['table'][$row]['avg_year'] = [
            '#markup' => !empty($results['avg_year']) ? number_format($results['avg_year'], 2, '.', '') : '-',
          ];
          $build[$key]['table'][$row]['total_length'] = [
            '#markup' => !empty($results['sum_length']) ? $this->albumApi->formatTime((int) $results['sum_length']) : '-',
          ];
          $build[$key]['table'][$row]['avg_length'] = [
            '#markup' => !empty($results['avg_length']) ? $this->albumApi->formatTime((int) $results['avg_length']) : '-',
          ];
          $build[$key]['table'][$row]['total_tracks'] = [
            '#markup' => !empty($results['sum_tracks']) ? $results['sum_tracks'] : '-',
          ];
          $build[$key]['table'][$row]['avg_tracks'] = [
            '#markup' => !empty($results['avg_tracks']) ? number_format($results['avg_tracks'], 2) : '-',
          ];
          $build[$key]['table'][$row]['total_best_year'] = [
            '#markup' => !empty($results['count_best_year']) ? $results['count_best_year'] : '-',
          ];
          $build[$key]['table'][$row]['total_best_decade'] = [
            '#markup' => !empty($results['count_best_decade']) ? $results['count_best_decade'] : '-',
          ];
          $build[$key]['table'][$row]['total_best_all_time'] = [
            '#markup' => !empty($results['count_best_all_time']) ? $results['count_best_all_time'] : '-',
          ];
        }
      }
    }

    return $build;
  }

  /**
   * Get the date query parameters.
   *
   * @return array
   *   The date filters if they exist.
   */
  public function getDateFilters($incoming_format = 'Y-m-d') {
    $date_filters = [];
    foreach (['start', 'end'] as $key) {
      if ($date = $this->request->query->get($key)) {
        if ($incoming_format == 'Y') {
          $date = $key == 'start' ? $date . '-01-01' : $date . '-12-31';
        }
        $date_filters[$key] = date('Y-m-d', strtotime($date));
      }
    }
    return $date_filters;
  }

  /**
   * Get a list of decades.
   *
   * @param int $decade
   *   The minimum decade value that should be in the list.
   *
   * @return array
   *   An array of decades from the minimum up to today.
   */
  public function getDecades(int $decade = 1950) {
    $decades = [];
    if ($decade <= 1950) {
      $decades = [
        '< 1950' => [
          'start' => 1900,
          'end' => 1949,
        ],
      ];
    }
    while ($decade <= date('Y')) {
      $decades[$decade] = [
        'start' => $decade,
        'end' => $decade + 9,
      ];
      $decade += 10;
    }
    return $decades;
  }

}
