<?php

namespace Drupal\album\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a block for displaying links to manager pages.
 *
 * @Block(
 *   id = "album_manager_links",
 *   admin_label = @Translation("Album Manager Links"),
 *   category = @Translation("Album"),
 * )
 */
class ManagerLinks extends BlockBase implements BlockPluginInterface, ContainerFactoryPluginInterface {

  /**
   * The currently active request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected Request $request;

  /**
   * Creates a ManagerLinks instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RequestStack $request_stack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $all_time = NULL;
    $decade = NULL;
    $years = [];
    $start = $this->request->query->get('start');
    $facets = $this->request->query->get('f');
    if (!empty($facets)) {
      foreach ($facets as $facet) {
        $filters = explode(':', $facet);
        if ($filters[0] == 'year') {
          $years[] = $filters[1];
        }
        if ($filters[0] == 'best_of_all_time') {
          $all_time = TRUE;
        }
      }
    }
    if (!empty($start)) {
      $decade = (int) floor($start * 0.1) * 10;
    }

    $build = [];
    $build['#cache']['tags'] = ['node_type:album'];
    $build['#cache']['contexts'] = [
      'url.path',
      'url.query_args:start',
      'url.query_args:end',
      'url.query_args:f',
    ];
    $build['list'] = [
      '#theme' => 'item_list',
      '#items' => [],
    ];

    if (!empty($all_time)) {
      $build['list']['#items'][] = [
        '#title' => $this->t('Manage best albums of all time'),
        '#type' => 'link',
        '#url' => Url::fromRoute('album.best_all_time_manager'),
      ];
    }
    if (!empty($decade)) {
      $build['list']['#items'][] = [
        '#title' => $this->t('Manage best albums of @decade', ['@decade' => $decade . 's']),
        '#type' => 'link',
        '#url' => Url::fromRoute('album.best_decade_manager', ['decade' => $decade]),
      ];
    }
    if (!empty($years)) {
      foreach ($years as $year) {
        $build['list']['#items'][] = [
          '#title' => $this->t('Manage best albums of @year', ['@year' => $year]),
          '#type' => 'link',
          '#url' => Url::fromRoute('album.best_year_manager', ['year' => $year]),
        ];
      }
    }
    return !empty($build['list']['#items']) ? $build : [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
