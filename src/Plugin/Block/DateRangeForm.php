<?php

namespace Drupal\album\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block searching a date range.
 *
 * @Block(
 *   id = "album_date_range_form",
 *   admin_label = @Translation("Album Date Range Form"),
 *   category = @Translation("Album"),
 * )
 */
class DateRangeForm extends BlockBase implements BlockPluginInterface, ContainerFactoryPluginInterface {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected FormBuilderInterface $formBuilder;

  /**
   * Creates a DateRangeForm instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    return [
      'album_date_range_form' => $this->formBuilder->getForm('Drupal\album\Form\DateRange', [], $config['select_type'], $config['namespace']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['select_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Range Type'),
      '#options' => [
        'dates' => $this->t('Start and End Dates'),
        'decades' => $this->t('Decades List'),
        'views_range' => $this->t('Views Range'),
      ],
      '#default_value' => $config['select_type'] ?? 'dates',
    ];

    $form['namespace'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Submit Value Namespace'),
      '#default_value' => $config['namespace'] ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['select_type'] = $values['select_type'];
    $this->configuration['namespace'] = $values['namespace'];
  }

}
