<?php

namespace Drupal\album\Plugin\Block;

use Drupal\facets\Plugin\Block\FacetBlock;

/**
 * Provides a block for all 'best of' facets.
 *
 * @Block(
 *   id = "album_best_of_facet",
 *   admin_label = @Translation("Best of Facet"),
 *   category = @Translation("Album"),
 * )
 */
class BestOfFacet extends FacetBlock {

  /**
   * {@inheritdoc}
   */
  public function build() {
    /** @var \Drupal\facets\FacetInterface $facet */
    $full_build = [];
    $facet_ids = [
      'best_of_all_time',
      'best_of_decade',
      'best_of_year_node',
    ];
    foreach ($facet_ids as $facet_id) {
      $facet = $this->facetStorage->load($facet_id);

      // No need to build the facet if it does not need to be visible.
      if ($facet->getOnlyVisibleWhenFacetSourceIsVisible() &&
        (!$facet->getFacetSource() || !$facet->getFacetSource()->isRenderedInCurrentRequest())) {
        return [];
      }

      // Let the facet_manager build the facets.
      $build = $this->facetManager->build($facet);

      if (!empty($build)) {
        // Add extra elements from facet source, for example, ajax scripts.
        // @see Drupal\facets\Plugin\facets\facet_source\SearchApiDisplay
        /** @var \Drupal\facets\FacetSource\FacetSourcePluginInterface $facet_source */
        $facet_source = $facet->getFacetSource();
        $build += $facet_source->buildFacet();

        // Add contextual links only when we have results.
        $build['#contextual_links']['facets_facet'] = [
          'route_parameters' => ['facets_facet' => $facet->id()],
        ];

        // Add classes needed for ajax.
        if (!empty($build['#use_ajax'])) {
          $build['#attributes']['class'][] = 'block-facets-ajax';
          // The configuration block id isn't always set in the configuration.
          if (isset($this->configuration['block_id'])) {
            $build['#attributes']['class'][] = 'js-facet-block-id-' . $this->configuration['block_id'];
          }
          else {
            $build['#attributes']['class'][] = 'js-facet-block-id-' . $this->pluginId;
          }
        }
        $full_build[$facet_id] = $build;
      }
    }

    return $full_build;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    /** @var \Drupal\facets\FacetInterface $facet */
    // @todo Should all 3 facets be the dependency?
    $facet = $this->facetStorage->load('best_of_all_time');
    return ['config' => [$facet->getConfigDependencyName()]];
  }

}
