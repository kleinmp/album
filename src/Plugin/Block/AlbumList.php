<?php

namespace Drupal\album\Plugin\Block;

use Drupal\album\AlbumApi;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block listing albums.
 *
 * @Block(
 *   id = "album_list",
 *   admin_label = @Translation("Album List"),
 *   category = @Translation("Album"),
 * )
 */
class AlbumList extends BlockBase implements BlockPluginInterface, ContainerFactoryPluginInterface {

  /**
   * The album api.
   *
   * @var \Drupal\album\AlbumApi
   */
  protected AlbumApi $albumApi;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Creates a AlbumList instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\album\AlbumApi $album_api
   *   The album api.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AlbumApi $album_api, RouteMatchInterface $route_match, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->albumApi = $album_api;
    $this->routeMatch = $route_match;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('album.album_api'),
      $container->get('current_route_match'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $options = [
      'order' => $config['order'],
      'count' => $config['count'],
      'load' => TRUE,
    ];
    if ($node = $this->routeMatch->getParameter('node')) {
      switch ($node->bundle()) {
        case 'album':
          $options['skip_albums'] = [$node->id()];
          if ($node->field_artist_to_album->entity) {
            $options['artist_id'] = $node->field_artist_to_album->entity->id();
          }
          break;

        case 'artist':
          $options['artist_id'] = $node->id();
          break;
      }
    }

    $albums = $this->albumApi->getAlbums($options);

    if (!empty($albums)) {
      foreach ($albums as $nid => $album) {
        $view_builder = $this->entityTypeManager->getViewBuilder('node');
        $build[$nid] = $view_builder->view($album, $config['view_mode']);
      }
    }
    $build['#cache'] = [
      'max-age' => 3600,
      'contexts' => [
        'route.artist_node',
      ],
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['count'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of albums to show.'),
      '#description' => $this->t('Choose 0 for unlimited'),
      '#default_value' => $config['count'] ?? 2,
    ];
    $form['order'] = [
      '#type' => 'select',
      '#title' => $this->t('Order'),
      '#description' => $this->t('The order in which the orders will be listed.'),
      '#options' => [
        'rand' => $this->t('Random'),
        'field_received' => $this->t('Date Received'),
        'field_year' => $this->t('Year'),
      ],
      '#default_value' => $config['order'] ?? 'rand',
    ];
    $form['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('View Mode'),
      '#options' => [
        'default' => $this->t('Default'),
        'full' => $this->t('Full'),
        'teaser' => $this->t('Teaser'),
      ],
      '#default_value' => $config['view_mode'] ?? 0,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    foreach (['count', 'order', 'view_mode'] as $option) {
      $this->configuration[$option] = $values[$option];
    }
  }

}
