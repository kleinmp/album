<?php

namespace Drupal\album\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Url;

/**
 * Display link to filtered album page.
 *
 * @FieldFormatter(
 *   id = "term_link",
 *   label = @Translation("Album Term Link"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class TermLink extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Displays links to the albums page with the tag selected.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    // e.g. https://www.example.com/albums?f[0]=tags:42
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      $id = Xss::filter($entity->id());
      $element[$delta] = [
        '#title' => $entity->label(),
        '#type' => 'link',
        '#url' => Url::fromRoute('view.albums.albums_page', [], [
          'query' => [
            'f' => [
              "tags:$id",
            ],
          ],
        ]),
      ];
    }

    return $element;
  }

}
