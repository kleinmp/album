<?php

namespace Drupal\album\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Url;

/**
 * Display link to albums page with year selected.
 *
 * @FieldFormatter(
 *   id = "year_link",
 *   label = @Translation("Album Year Link"),
 *   field_types = {
 *     "integer",
 *     "string"
 *   }
 * )
 */
class YearLink extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Displays links to the albums page with the year selected.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $year = Xss::filter($item->value);
      $element[$delta] = [
        '#title' => $year,
        '#type' => 'link',
        '#url' => Url::fromRoute('view.albums.albums_page', [], [
          'query' => [
            'f' => [
              "year:$year",
            ],
          ],
        ]),
      ];
    }

    return $element;
  }

}
