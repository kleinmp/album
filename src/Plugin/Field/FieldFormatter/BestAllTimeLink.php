<?php

namespace Drupal\album\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Url;

/**
 * Display link to albums page with best-of-all-time selected.
 *
 * @FieldFormatter(
 *   id = "best_all_time_link",
 *   label = @Translation("Best All Time Link"),
 *   field_types = {
 *     "integer"
 *   }
 * )
 */
class BestAllTimeLink extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Display link to albums page with best-of selected.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $query = $this->getQuery($items);
    foreach ($items as $delta => $item) {
      $element[$delta] = [
        '#title' => Xss::filter($item->value),
        '#type' => 'link',
        '#url' => Url::fromRoute('view.albums.albums_page', [], ['query' => $query]),
      ];
    }

    return $element;
  }

  /**
   * Get the url query to append to the link.
   *
   * @param Drupal\Core\Field\FieldItemListInterface $items
   *   The field items to be displayed.
   *
   * @return array
   *   An array to be used as the query params in the displayed url.
   *
   * @see Drupal\Core\Url::fromRoute
   */
  public function getQuery(FieldItemListInterface $items) {
    // https://www.example.com/albums?f[0]=best_of_all_time:1&order=field_best_all_time_int&sort=asc
    return [
      'f' => [
        'best_of_all_time:1',
      ],
      'order' => 'field_best_all_time_int',
      'sort' => 'asc',
    ];
  }

}
