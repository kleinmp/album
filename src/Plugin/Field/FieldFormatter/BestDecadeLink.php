<?php

namespace Drupal\album\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Display link to albums page with best-decade selected.
 *
 * @FieldFormatter(
 *   id = "best_decade_link",
 *   label = @Translation("Best Decade Link"),
 *   field_types = {
 *     "integer"
 *   }
 * )
 */
class BestDecadeLink extends BestAllTimeLink {

  /**
   * {@inheritdoc}
   */
  public function getQuery(FieldItemListInterface $items) {
    // https://www.example.com/albums?start=2000-01-01&end=2009-12-31&&f[0]=best_of_decade:1&order=field_best_decade_int&sort=asc
    $entity = $items->getEntity();
    $year = $entity->get('field_year')->value;
    $decade_substr = substr($year, 0, 3);
    return [
      'f' => [
        'best_of_decade:1',
      ],
      'order' => 'field_best_decade_int',
      'sort' => 'asc',
      'start' => Xss::filter($decade_substr . '0 01-01'),
      'end' => Xss::filter($decade_substr . '9 12-31'),
    ];
  }

}
