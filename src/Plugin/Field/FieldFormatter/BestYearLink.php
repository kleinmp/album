<?php

namespace Drupal\album\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Display link to albums page with best-year selected.
 *
 * @FieldFormatter(
 *   id = "best_year_link",
 *   label = @Translation("Best Year Link"),
 *   field_types = {
 *     "integer"
 *   }
 * )
 */
class BestYearLink extends BestAllTimeLink {

  /**
   * {@inheritdoc}
   */
  public function getQuery(FieldItemListInterface $items) {
    // https://www.example.com/albums?f[0]=best_of_year_node:1&f[1]=year:2019&order=field_best_year_int&sort=asc
    $entity = $items->getEntity();
    return [
      'f' => [
        'best_of_year_node:1',
        'year:' . Xss::filter($entity->get('field_year')->value),
      ],
      'order' => 'field_best_year_int',
      'sort' => 'asc',
    ];
  }

}
