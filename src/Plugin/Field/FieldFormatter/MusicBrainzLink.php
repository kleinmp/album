<?php

namespace Drupal\album\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Display link to Musicbrainz api.
 *
 * @FieldFormatter(
 *   id = "MusicBrainzLink",
 *   label = @Translation("MusicBrainz Link"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class MusicBrainzLink extends FormatterBase {

  const MB_URL = 'https://musicbrainz.org/';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'entity_type' => 'release',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['entity_type'] = [
      '#title' => $this->t('MB Entity Type'),
      '#type' => 'select',
      '#options' => [
        'artist' => $this->t('Artist'),
        'recording' => $this->t('Track'),
        'release' => $this->t('Album'),
      ],
      '#default_value' => $this->getSetting('entity_type'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Displays links to @entity_type entities in Music Brainz.', ['@entity_type' => $this->getSetting('entity_type')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $ids = explode(', ', $item->value);
      $url = Url::fromUri(self::MB_URL . $this->getSetting('entity_type') . '/' . reset($ids));
      $element[$delta] = [
        '#title' => $this->t('Music Brainz'),
        '#type' => 'link',
        '#url' => $url,
        '#attributes' => ['target' => '_blank'],
      ];
    }

    return $element;
  }

}
