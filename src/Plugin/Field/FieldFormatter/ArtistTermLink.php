<?php

namespace Drupal\album\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Url;

/**
 * Display link to filtered artist page.
 *
 * @FieldFormatter(
 *   id = "artist_term_link",
 *   label = @Translation("Artist Term Link"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class ArtistTermLink extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Displays links to the artist page with the tag selected.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    // e.g. https://www.example.com/artists?f[0]=tags:14
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      $id = Xss::filter($entity->id());
      $element[$delta] = [
        '#title' => $entity->label(),
        '#type' => 'link',
        '#url' => Url::fromRoute('view.artists.artists_page', [], [
          'query' => [
            'f' => [
              "tags:$id",
            ],
          ],
        ]),
      ];
    }

    return $element;
  }

}
