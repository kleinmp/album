<?php

namespace Drupal\album\Plugin\facets\processor;

use Drupal\Core\TypedData\ComplexDataDefinitionInterface;
use Drupal\facets\FacetInterface;
use Drupal\facets\Processor\BuildProcessorInterface;
use Drupal\facets\Plugin\facets\processor\BooleanItemProcessor;

/**
 * Provides a processor for boolean labels to integer data.
 *
 * @FacetsProcessor(
 *   id = "integer_boolean_item",
 *   label = @Translation("Integer boolean item label"),
 *   description = @Translation("Display configurable On/Off labels instead 1/0 values for boolean fields."),
 *   stages = {
 *     "build" = 35
 *   }
 * )
 */
class IntegerBooleanItemProcessor extends BooleanItemProcessor implements BuildProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet, array $results) {
    $config = $this->getConfiguration();
    if (empty($config['off_value']) && empty($config['on_value'])) {
      return $results;
    }

    /** @var \Drupal\facets\Result\Result $result */
    foreach ($results as $key => $result) {
      $value = '';
      if ($result->getRawValue() == 0) {
        $value = $config['off_value'];
      }
      elseif ($result->getRawValue() == 1) {
        $value = $config['on_value'];
      }
      if ($value == '') {
        unset($results[$key]);
      }
      else {
        $result->setDisplayValue($value);
      }
    }

    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function supportsFacet(FacetInterface $facet) {
    $data_definition = $facet->getDataDefinition();
    if ($data_definition->getDataType() == "integer") {
      return TRUE;
    }
    if (!($data_definition instanceof ComplexDataDefinitionInterface)) {
      return FALSE;
    }

    $property_definitions = $data_definition->getPropertyDefinitions();
    foreach ($property_definitions as $definition) {
      if ($definition->getDataType() == "integer") {
        return TRUE;
      }
    }
    return FALSE;
  }

}
