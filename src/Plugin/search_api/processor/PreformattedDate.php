<?php

namespace Drupal\album\Plugin\search_api\processor;

use Drupal\album\Plugin\search_api\processor\Property\PreformattedDateProperty;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Utility\Utility;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Adds preformatted date from existing fields to the index.
 *
 * @see \Drupal\album\Plugin\search_api\processor\Property\PreformattedDateProperty
 *
 * @SearchApiProcessor(
 *   id = "preformatted_date",
 *   label = @Translation("Preformatted Date"),
 *   description = @Translation("Add a preformatted date from a date field to the index."),
 *   stages = {
 *     "add_properties" = 20,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class PreformattedDate extends ProcessorPluginBase {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $processor */
    $processor = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $processor->setDateFormatter($container->get('date.formatter'));
    return $processor;
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Preformatted Date'),
        'description' => $this->t('Preformatted date from date field.'),
        'type' => 'string',
        'processor_id' => $this->getPluginId(),
        'is_list' => FALSE,
      ];
      $properties['preformatted_date'] = new PreformattedDateProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    $preformatted_date_fields = $this->getFieldsHelper()
      ->filterForPropertyPath($item->getFields(), NULL, 'preformatted_date');
    $required_properties_by_datasource = [
      NULL => [],
      $item->getDatasourceId() => [],
    ];
    foreach ($preformatted_date_fields as $field) {
      $configuration = $field->getConfiguration();
      $combined_id = $configuration['field'];
      [$datasource_id, $property_path] = Utility::splitCombinedId($combined_id);
      $required_properties_by_datasource[$datasource_id][$property_path] = $combined_id;
      $property_values = $this->getFieldsHelper()
        ->extractItemValues([$item], $required_properties_by_datasource)[0];
      if ($timestamp = reset($property_values[$combined_id])) {
        $formatted = $this->getDateFormatter()->format($timestamp, 'custom', $configuration['format']);
        // Do not use setValues(), since that doesn't preprocess the values
        // according to their data type.
        $field->addValue($formatted);
      }
    }
  }

  /**
   * Sets the date formatter service to use for this plugin.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   *
   * @return $this
   */
  public function setDateFormatter(DateFormatterInterface $date_formatter) {
    $this->dateFormatter = $date_formatter;
    return $this;
  }

  /**
   * Retrieves the date formatter service.
   *
   * @return \Drupal\Core\Datetime\DateFormatterInterface
   *   The date formatter service.
   */
  public function getDateFormatter() {
    return $this->dateFormatter ?: \Drupal::service('date.formatter');
  }

}
