<?php

namespace Drupal\album\Plugin\views\field;

use Drupal\Component\Utility\Xss;
use Drupal\views\ResultRow;
use Drupal\views\Plugin\views\field\Boolean;
use Drupal\views\Render\ViewsRenderPipelineMarkup;

/**
 * A handler to provide proper displays for best track on an album.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("album_view_best_track")
 */
class AlbumViewBestTrack extends Boolean {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing since this is computed.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $value = FALSE;
    $node = $this->getEntity($values);
    if (is_object($node) && get_class($node) === 'Drupal\node\Entity\Node' && $node->hasField('best_track')) {
      $value = $node->get('best_track')->first()->getValue()['value'];
    }

    if (!empty($this->options['not'])) {
      $value = !$value;
    }

    if ($this->options['type'] == 'custom') {
      $custom_value = $value ? $this->options['type_custom_true'] : $this->options['type_custom_false'];
      return ViewsRenderPipelineMarkup::create(Xss::filterAdmin($custom_value));
    }
    elseif (isset($this->formats[$this->options['type']])) {
      return $value ? $this->formats[$this->options['type']][0] : $this->formats[$this->options['type']][1];
    }
    else {
      return $value ? $this->formats['yes-no'][0] : $this->formats['yes-no'][1];
    }
  }

}
