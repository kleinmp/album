<?php

namespace Drupal\album;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Item list for a computed field that displays number of best tracks on album.
 *
 * In practice it should always be 0 or 1.
 */
class AlbumBestTrackItemList extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    $this->list[0] = $this->createItem(0, $this->bestTrackCount());
  }

  /**
   * {@inheritdoc}
   */
  protected function ensureComputedValue() {
    // Compute it each time instead of caching.
    $this->computeValue();
  }

  /**
   * Compute the number of best tracks on album.
   *
   * It should always be 0 or 1 in practice.
   *
   * @return int
   *   Number of best tracks on album.
   */
  protected function bestTrackCount() {
    $count = 0;
    $entity = $this->getEntity();
    $tracks = $entity->get('field_tracks')->referencedEntities();

    if (!empty($tracks)) {
      foreach ($tracks as $track) {
        if ($track->get('field_best_of_year')->value) {
          $count++;
        }
      }
    }
    return $count;
  }

}
