<?php

namespace Drupal\album;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Item list for a computed field that displays the score on an artist.
 */
class ArtistScoreItemList extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * The album api.
   *
   * @var \Drupal\album\AlbumApi
   */
  protected AlbumApi $albumApi;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    $this->list[0] = $this->createItem(0, $this->getArtistScore());
  }

  /**
   * {@inheritdoc}
   */
  protected function ensureComputedValue() {
    // Compute it each time instead of caching.
    $this->computeValue();
  }

  /**
   * Get the score on an album.
   *
   * @return float
   *   The computed artist score.
   */
  protected function getArtistScore() : float {
    $entity = $this->getEntity();
    return $this->albumApi()->computeArtistScore($entity);
  }

  /**
   * Get the album api service.
   *
   * @return Drupal\album\AlbumApi
   *   The album api service.
   */
  protected function albumApi() : AlbumApi {
    // @todo Dependency injection is not available for TypedData yet:
    // https://www.drupal.org/project/drupal/issues/2053415
    if (empty($this->albumApi)) {
      $this->albumApi = \Drupal::service('album.album_api');
    }
    return $this->albumApi;
  }

}
