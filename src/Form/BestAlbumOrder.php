<?php

namespace Drupal\album\Form;

use Drupal\album\AlbumApi;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Best Albums order management form.
 */
class BestAlbumOrder extends FormBase {

  /**
   * The album api.
   *
   * @var \Drupal\album\AlbumApi
   */
  protected $albumApi;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Creates a BestAlbumOrder instance.
   *
   * @param \Drupal\album\AlbumApi $album_api
   *   The album api.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    AlbumApi $album_api,
    EntityTypeManagerInterface $entity_type_manager,
  ) {
    $this->albumApi = $album_api;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('album.album_api'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'album_best_album_order';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $field = NULL, array $options = []) {
    // https://api.drupal.org/api/examples/tabledrag_example%21src%21Form%21TableDragExampleSimpleForm.php/8.x-1.x
    $options += [
      'negate' => FALSE,
    ];
    $album_list = $this->albumApi->getBestAlbums($field, $options);

    $year = !empty($options['years']['start']) ? $options['years']['start'] : 0;
    $form['message'] = [
      '#markup' => $this->t('You may add a maximum of %count best albums', ['%count' => $this->albumApi->getMaxBestOfCount($field, $year)]),
    ];
    $form['table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Title'),
        '',
        $this->t('Artist'),
        $this->t('Year'),
        $this->t('Best of Year'),
        $this->t('Best of Decade'),
        $this->t('Best of All'),
        // Remove.
        '',
      ],
      '#empty' => $this->t('There are no albums yet.'),
      // TableDrag: Each array value is a list of callback arguments for
      // drupal_add_tabledrag(). The #id of the table is automatically
      // prepended; if there is none, an HTML ID is auto-generated.
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'table-sort-weight',
        ],
      ],
    ];

    if (!empty($album_list)) {
      $delta = ceil(count($album_list) * 0.5);
      foreach ($album_list as $album) {
        $artist = $album->get('field_artist_to_album')->entity;
        $form['table'][$album->id()]['#attributes']['class'][] = 'draggable';
        $form['table'][$album->id()]['#weight'] = $album->get($field)->value - 1;

        $form['table'][$album->id()]['title'] = [
          'cover' => [
            '#theme' => 'image_style',
            '#style_name' => 'small',
            '#uri' => $album->get('field_album_cover')->entity->get('uri')->value,
          ],
          'title' => $album->toLink(NULL, 'canonical', ['target' => '_blank'])->toRenderable(),
        ];
        $form['table'][$album->id()]['weight'] = [
          '#type' => 'weight',
          '#title' => $this->t('Number'),
          '#title_display' => 'invisible',
          '#default_value' => $album->get($field)->value - 1,
          '#delta' => $delta,
          '#attributes' => [
            'class' => [
              'table-sort-weight',
            ],
          ],
        ];
        $form['table'][$album->id()]['artist'] = $artist->toLink(NULL, 'canonical', ['target' => '_blank'])->toRenderable();
        $form['table'][$album->id()]['year'] = [
          '#markup' => Xss::filter((string) $album->get('field_year')->value),
        ];
        $form['table'][$album->id()]['best_of_year'] = [
          '#markup' => Xss::filter((string) $album->get('field_best_year')->value),
        ];
        $form['table'][$album->id()]['best_of_decade'] = [
          '#markup' => Xss::filter((string) $album->get('field_best_decade')->value),
        ];
        $form['table'][$album->id()]['best_of_all'] = [
          '#markup' => Xss::filter((string) $album->get('field_best_all_time')->value),
        ];
        $form['table'][$album->id()]['remove'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Remove'),
          '#default_value' => FALSE,
        ];
      }
    }
    $form['field'] = [
      '#type' => 'value',
      '#value' => $field,
    ];
    $form['options'] = [
      '#type' => 'value',
      '#value' => $options,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Re-order'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (!empty($values['table'])) {
      $number = 1;
      uasort($values['table'], function ($a, $b) {
        return $a['weight'] <=> $b['weight'];
      });
      foreach ($values['table'] as $album_id => $data) {
        $album = $this->entityTypeManager->getStorage('node')->load($album_id);
        if (empty($data['remove'])) {
          $album->set($values['field'], $number);
          $number++;
        }
        else {
          $album->set($values['field'], NULL);
        }
        $album->save();
      }
    }
  }

}
