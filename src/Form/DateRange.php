<?php

namespace Drupal\album\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * DateRange select form.
 */
class DateRange extends FormBase {

  /**
   * The currently active request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Keep track of the versions of this form on page.
   *
   * @var int
   */
  private static $count = 0;

  /**
   * Creates a DateRange instance.
   *
   * @param Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   */
  public function __construct(RequestStack $request_stack, RouteMatchInterface $route_match) {
    $this->request = $request_stack->getCurrentRequest();
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    self::$count++;
    return 'album_date_range_' . self::$count;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $date_filters = [], $select_type = 'dates', $namespace = '') {
    $start = $namespace . 'start';
    $end = $namespace . 'end';
    $date_filters = [$start => NULL, $end => NULL];
    foreach ([$start, $end] as $key) {
      if ($this->request->query->get($key)) {
        $date_filters[$key] = $this->request->query->get($key);
      }
    }
    if (!empty($namespace)) {
      $date_filters[$namespace] = $this->request->query->get($namespace);
    }
    if ($select_type == 'decades') {
      $decade = 1920;
      $decades = [
        'All' => $this->t('All'),
      ];
      while ($decade <= date('Y')) {
        $decades[$decade] = $decade . 's';
        $decade += 10;
      }

      $default_decade = 'All';
      if (!empty($date_filters['start']) && !empty($date_filters['end'])) {
        $start_date = (int) $date_filters['start'];
        $end_date = (int) $date_filters['end'];
        if ($end_date - $start_date == 9 && $start_date % 10 === 0) {
          $default_decade = $start_date;
        }
      }
      $form['decade'] = [
        '#type' => 'select',
        '#options' => $decades,
        '#default_value' => $default_decade,
      ];
    }
    else {
      // @todo At least one of start or end is required.
      $form[$start] = [
        '#title' => $this->t('Start Date'),
        '#type' => 'date',
        '#default_value' => $date_filters[$start],
      ];
      $form[$end] = [
        '#title' => $this->t('End Date'),
        '#type' => 'date',
        '#default_value' => $date_filters[$end],
      ];
    }
    if ($select_type == 'views_range' && !empty($date_filters[$namespace])) {
      $form[$start]['#default_value'] = substr($date_filters[$namespace]['min'], 0, 10);
      $form[$end]['#default_value'] = substr($date_filters[$namespace]['max'], 0, 10);
    }

    $form['select_type'] = [
      '#type' => 'value',
      '#value' => $select_type,
    ];
    $form['namespace'] = [
      '#type' => 'value',
      '#value' => $namespace,
    ];
    $form[$namespace . 'submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $query = [];
    $namespace = $values['namespace'];
    $start = $namespace . 'start';
    $end = $namespace . 'end';

    if ($values['select_type'] == 'decades') {
      if ($values['decade'] != 'All') {
        $query[$start] = (int) $values['decade'];
        $query[$end] = $query[$start] + 9;
      }
    }
    elseif ($values['select_type'] == 'views_range') {
      $query[$namespace]['min'] = $values[$start] . ' 00:00:00';
      if (empty($values[$end])) {
        $values[$end] = date('Y-m-d');
      }
      $query[$namespace]['max'] = $values[$end] . ' 23:59:59';
    }
    else {
      $query[$start] = $values[$start];
      $query[$end] = $values[$end];
    }

    foreach ($this->request->query->all() as $key => $value) {
      if (!in_array($key, [$start, $end, $namespace])) {
        $query[$key] = $value;
      }
    }

    $route_name = $this->routeMatch->getRouteName();
    $url = Url::fromRoute($route_name, $query);
    $form_state->setRedirectUrl($url);
  }

}
