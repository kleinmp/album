<?php

namespace Drupal\album\Form;

use Drupal\album\AlbumApi;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Best Albums addition management form.
 */
class BestAlbumAdd extends FormBase {

  /**
   * The album api.
   *
   * @var \Drupal\album\AlbumApi
   */
  protected AlbumApi $albumApi;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Creates a BestAlbumAdd instance.
   *
   * @param \Drupal\album\AlbumApi $album_api
   *   The album api.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    AlbumApi $album_api,
    EntityTypeManagerInterface $entity_type_manager,
  ) {
    $this->albumApi = $album_api;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('album.album_api'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'album_best_album_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $field = NULL, array $options = []) {
    $options += [
      'negate' => TRUE,
    ];
    $album_options = [];
    $album_list = $this->albumApi->getBestAlbums($field, $options);
    if (!empty($album_list)) {
      foreach ($album_list as $album) {
        $title = $this->t('@title by @artist (@year)', [
          '@title' => $album->get('title')->value,
          '@artist' => $album->get('field_artist_to_album')->entity->get('title')->value,
          '@year' => $album->get('field_year')->value,
        ]);
        if (!empty($options['order']) && $options['order'] != 'title') {
          if ($album->get($options['order'])->value) {
            $title .= $this->t(': @value', ['@value' => $album->get($options['order'])->value]);
          }
        }
        $album_options[$album->id()] = $title;
      }
    }
    $form['albums'] = [
      '#title' => $this->t('Albums'),
      '#type' => 'select',
      '#options' => $album_options,
      '#multiple' => TRUE,
      '#size' => 15,
    ];
    $form['field'] = [
      '#type' => 'value',
      '#value' => $field,
    ];
    $form['options'] = [
      '#type' => 'value',
      '#value' => $options,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $best_rank = 1;
    $options = $values['options'];
    $options['negate'] = FALSE;
    $options['order'] = $values['field'];
    if ($album_list = $this->albumApi->getBestAlbums($values['field'], $options)) {
      $best_rank += end($album_list)->get($values['field'])->value;
    }
    if (!empty($values['albums'])) {
      foreach ($values['albums'] as $album_id) {
        $album = $this->entityTypeManager->getStorage('node')->load($album_id);
        $album->set($values['field'], $best_rank);
        $album->save();
        $best_rank++;
      }
    }
  }

}
