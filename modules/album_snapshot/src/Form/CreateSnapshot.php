<?php

namespace Drupal\album_snapshot\Form;

use Drupal\album_snapshot\SnapshotApi;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for creating snapshots.
 */
class CreateSnapshot extends FormBase {

  /**
   * The snapshot api.
   *
   * @var \Drupal\album_snapshot\SnapshotApi
   */
  protected $snapshotApi;

  /**
   * Creates a CreateSnapshot instance.
   *
   * @param \Drupal\album_snapshot\SnapshotApi $snapshot_api
   *   The snapshot api.
   */
  public function __construct(SnapshotApi $snapshot_api) {
    $this->snapshotApi = $snapshot_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('album_snapshot.snapshot_api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'album_snapshot_create_snapshot';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['message'] = [
      '#markup' => $this->t('This will create a snapshot of the best albums at this current date.'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $snapshot = $this->snapshotApi->createSnapshotNode();
    $url = Url::fromUri('entity:node/' . $snapshot->id());
    $form_state->setRedirectUrl($url);
  }

}
