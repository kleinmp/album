<?php

namespace Drupal\album_snapshot;

use Drupal\album\AlbumApi;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\Entity\Node;

/**
 * Service api for snapshots.
 */
class SnapshotApi {

  /**
   * The album api.
   *
   * @var \Drupal\album\AlbumApi
   */
  protected AlbumApi $albumApi;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * AlbumImportApi constructor.
   *
   * @param \Drupal\album\AlbumApi $album_api
   *   The album api.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger channel.
   */
  public function __construct(
    AlbumApi $album_api,
    EntityTypeManagerInterface $entity_type_manager,
    LoggerChannelInterface $logger,
  ) {
    $this->albumApi = $album_api;
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
  }

  /**
   * Create new snapshot node.
   *
   * @return \Drupal\node\Entity\Node
   *   The snapshot that was created.
   */
  public function createSnapshotNode() : Node {
    $node = $this->entityTypeManager->getStorage('node')->create([
      'type' => 'snapshot',
      'title' => date('m-d-Y'),
      'uid' => '1',
      'status' => 1,
    ]);
    $albums = $this->albumApi->getBestAlbums('field_best_all_time');
    if (!empty($albums)) {
      $fields = [];
      foreach ($albums as $nid => $album) {
        $fields[] = ['target_id' => $nid];
      }
      $node->set('field_albums', $fields);
      $node->set('field_date', date('Y-m-d'));
      $node->save();
    }
    return $node;
  }

}
