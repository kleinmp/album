<?php

namespace Drupal\album_d6migrate\Plugin\migrate\destination;

use Drupal\migrate\Plugin\migrate\destination\EntityContentBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\migrate\Row;

/**
 * Custom paragraph destination.
 *
 * @MigrateDestination(
 *   id = "entity:paragraph"
 * )
 */
class Paragraph extends EntityContentBase {

  /**
   * The delta value of paragraph on the field associating it with the parent.
   *
   * @var int
   */
  protected $parentDelta = 0;

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    if (!empty($this->configuration['parent_delta_field'])) {
      $this->parentDelta = $row->getDestinationProperty($this->configuration['parent_delta_field']);
    }
    return parent::import($row, $old_destination_id_values);
  }

  /**
   * {@inheritdoc}
   */
  protected function save(ContentEntityInterface $entity, array $old_destination_id_values = []) {
    $result = parent::save($entity, $old_destination_id_values);
    // Set the reference on the parent field.
    if ($parent = $entity->getParentEntity()) {
      $field_values = $parent->get($entity->parent_field_name->value)->getValue();
      $field_values[$this->parentDelta] = [
        'target_id' => $entity->id(),
        'target_revision_id' => $entity->revision_id->value,
      ];
      $parent->set($entity->parent_field_name->value, $field_values, TRUE);
      $parent->save();
    }
    return $result;
  }

}
