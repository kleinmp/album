<?php

namespace Drupal\album_d6migrate\Plugin\migrate\source;

use Drupal\taxonomy\Plugin\migrate\source\d6\TermNode as TermNodeBase;

/**
 * Source returning tids from the term_node table for the current revision.
 *
 * @MigrateSource(
 *   id = "d6_fingerig_term_node",
 *   source_module = "taxonomy"
 * )
 */
class TermNode extends TermNodeBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    $query->condition('n.type', $this->configuration['node_type']);
    return $query;
  }

}
