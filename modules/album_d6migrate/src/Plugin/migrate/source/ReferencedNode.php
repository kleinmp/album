<?php

namespace Drupal\album_d6migrate\Plugin\migrate\source;

use Drupal\migrate\MigrateException;
use Drupal\migrate\Plugin\MigrateIdMapInterface;

/**
 * Drupal 6 node source from database as referenced from a field.
 *
 * @MigrateSource(
 *   id = "d6_referenced_node"
 * )
 */
class ReferencedNode extends Node {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $db = $this->getDatabase()->schema();
    $query = $this->select('node_revisions', 'nr');
    $query->innerJoin('node', 'n', static::JOIN);
    if (!isset($this->configuration['reference_field_name']) ||
       !isset($this->configuration['reference_field_table'])) {
      throw new MigrateException('You must define reference_field_name and reference_field_table to use a d6_referenced_node source.');
    }

    // Note: if the same node is referenced more than once,
    // then this join could be a problem.
    $reference_table = $this->configuration['reference_field_table'];
    $reference_column = $this->configuration['reference_field_name'] . '_nid';
    $full_ref_column = $query->escapeField("ref.$reference_column");

    $query->innerJoin($reference_table, 'ref', $full_ref_column . ' = n.nid');
    $query->innerJoin('node', 'node_ref', 'ref.vid = node_ref.vid');
    $this->handleTranslations($query);

    $query->fields('n', [
      'nid',
      'type',
      'language',
      'status',
      'created',
      'changed',
      'comment',
      'promote',
      'moderate',
      'sticky',
      'tnid',
      'translate',
    ])->fields('nr', [
      'title',
      'body',
      'teaser',
      'log',
      'timestamp',
      'format',
      'vid',
    ]);
    $query->addField('n', 'uid', 'node_uid');
    $query->addField('nr', 'uid', 'revision_uid');
    $query->addField('ref', 'nid', 'ref_nid');

    if ($db->fieldExists($reference_table, 'delta')) {
      $query->addField('ref', 'delta', 'ref_delta');
    }
    else {
      $query->addExpression(0, 'ref_delta');
    }

    $query->condition('n.type', $this->configuration['node_type']);
    $query->condition($full_ref_column, NULL, 'IS NOT NULL');

    if (isset($this->configuration['reference_node_type'])) {
      $query->condition('node_ref.type', $this->configuration['reference_node_type']);
    }
    $query->orderBy('ref_nid');
    $query->orderBy('ref_delta');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'ref_nid' => $this->t('The nid value of the node referencing this node.'),
      'ref_delta' => $this->t('The delta value of the node referencing field.'),
    ] + parent::fields();
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids = parent::getIds();
    $ids['ref_nid'] = [
      'type' => 'integer',
      'alias' => 'ref',
      'column_name' => 'nid',
    ];
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $this->prepareQuery();

    // The rules for determining what conditions to add to the query are as
    // follows (applying first applicable rule):
    // 1. If the map is joinable, join it. We will want to accept all rows
    //    which are either not in the map, or marked in the map as NEEDS_UPDATE.
    //    Note that if high water fields are in play, we want to accept all rows
    //    above the high water mark in addition to those selected by the map
    //    conditions, so we need to OR them together (but AND with any existing
    //    conditions in the query). So, ultimately the SQL condition will look
    //    like (original conditions) AND (map IS NULL OR map needs update
    //    OR above high water).
    $conditions = $this->query->orConditionGroup();
    $condition_added = FALSE;
    if (empty($this->configuration['ignore_map']) && $this->mapJoinable()) {
      // Build the join to the map table. Because the source key could have
      // multiple fields, we need to build things up.
      $count = 1;
      $map_join = '';
      $delimiter = '';
      foreach ($this->getIds() as $field_name => $field_schema) {
        // Edited from parent method:
        // We want to be able to use 2 columns with the same
        // name as the ids of this migration.
        // We had to add this conditional in order to do so.
        if (isset($field_schema['column_name'])) {
          $field_name = $field_schema['column_name'];
        }
        if (isset($field_schema['alias'])) {
          $field_name = $field_schema['alias'] . '.' . $this->query->escapeField($field_name);
        }
        $map_join .= "$delimiter$field_name = map.sourceid" . $count++;
        $delimiter = ' AND ';
      }

      $alias = $this->query->leftJoin($this->migration->getIdMap()->getQualifiedMapTableName(), 'map', $map_join);
      $conditions->isNull($alias . '.sourceid1');
      $conditions->condition($alias . '.source_row_status', MigrateIdMapInterface::STATUS_NEEDS_UPDATE);
      $condition_added = TRUE;

      // And as long as we have the map table, add its data to the row.
      $n = count($this->getIds());
      for ($count = 1; $count <= $n; $count++) {
        $map_key = 'sourceid' . $count;
        $this->query->addField($alias, $map_key, "migrate_map_$map_key");
      }
      if ($n = count($this->migration->getDestinationIds())) {
        for ($count = 1; $count <= $n; $count++) {
          $map_key = 'destid' . $count++;
          $this->query->addField($alias, $map_key, "migrate_map_$map_key");
        }
      }
      $this->query->addField($alias, 'source_row_status', 'migrate_map_source_row_status');
    }
    // 2. If we are using high water marks, also include rows above the mark.
    //    But, include all rows if the high water mark is not set.
    if ($this->getHighWaterProperty() && ($high_water = $this->getHighWater()) !== '') {
      $high_water_field = $this->getHighWaterField();
      $conditions->condition($high_water_field, $high_water, '>');
      $this->query->orderBy($high_water_field);
    }
    if ($condition_added) {
      $this->query->condition($conditions);
    }

    return new \IteratorIterator($this->query->execute());
  }

}
