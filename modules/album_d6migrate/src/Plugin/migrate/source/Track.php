<?php

namespace Drupal\album_d6migrate\Plugin\migrate\source;

use Drupal\node\Plugin\migrate\source\d6\Node as BaseNode;

/**
 * Drupal 6 track node source from database.
 *
 * @MigrateSource(
 *   id = "d6_track_node",
 *   source_module = "node"
 *
 * )
 */
class Track extends BaseNode {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();

    $query->innerJoin('content_type_track', 'tr', 'tr.vid = n.vid');
    $query->orderBy('field_album_to_track_nid', 'ASC');
    $query->orderBy('field_track_number_value', 'ASC');
    return $query;
  }

}
