<?php

namespace Drupal\album_d6migrate\Plugin\migrate\source;

use Drupal\node\Plugin\migrate\source\d6\Node as BaseNode;
use Drupal\migrate\Row;

/**
 * Drupal 6 node source from database as referenced from a field.
 *
 * @MigrateSource(
 *   id = "album_d6migrate_node"
 * )
 */
class Node extends BaseNode {

  /**
   * {@inheritdoc}
   */
  protected function getFieldInfo($node_type) {
    $db = $this->getDatabase()->schema();
    if (!isset($this->fieldInfo)) {
      $this->fieldInfo = [];

      // Query the database directly for all CCK field info.
      $query = $this->select('content_node_field_instance', 'cnfi');
      $query->join('content_node_field', 'cnf', 'cnf.field_name = cnfi.field_name');
      $query->fields('cnfi');
      $query->fields('cnf');

      // Don't grab backref field data because the table may not actually exist.
      $query->condition('cnfi.widget_type', 'noderelationships_backref', '!=');
      $query->condition('cnfi.widget_type', '', '!=');

      foreach ($query->execute() as $field) {
        $this->fieldInfo[$field['type_name']][$field['field_name']] = $field;
      }

      foreach ($this->fieldInfo as $type => $fields) {
        foreach ($fields as $field => $info) {
          foreach ($info as $property => $value) {
            if ($property == 'db_columns' || preg_match('/_settings$/', $property)) {
              $this->fieldInfo[$type][$field][$property] = unserialize($value);
            }
          }
        }
      }
    }

    // Check for workflow.
    if ($db->tableExists('workflow_states')) {
      $query = $this->select('workflows', 'w');
      $query->join('workflow_states', 'ws', 'ws.wid = w.wid');
      $query->fields('w');
      $query->fields('ws');
      foreach ($query->execute() as $workflow_state) {
        if (!isset($this->fieldInfo[$node_type]['workflow_' . $workflow_state['wid']])) {
          $this->fieldInfo[$node_type]['workflow_' . $workflow_state['wid']] = ['field_name' => 'workflow'];
        }
        $this->fieldInfo[$node_type]['workflow_' . $workflow_state['wid']][$workflow_state['sid']] = $workflow_state;
      }
    }

    return $this->fieldInfo[$node_type] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  protected function getCckData(array $field, Row $node) {
    // Load the workflow data if it exists.
    if ($field['field_name'] === 'workflow') {
      $query = $this->select('workflow_node', 'wn');
      return $query->fields('wn')
        ->condition('nid', $node->getSourceProperty('nid'))
        ->execute()
        ->fetchAll();
    }

    // The matrix field does not follow standard cck table structure.
    if ($field['widget_type'] === 'matrix') {
      $query = $this->select('node_field_matrix_data', 'md');
      return $query->fields('md')
        ->condition('vid', $node->getSourceProperty('vid'))
        ->condition('field_name', $field['field_name'])
        ->execute()
        ->fetchAll();
    }
    return parent::getCckData($field, $node);
  }

}
