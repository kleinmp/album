<?php

namespace Drupal\album_d6migrate\Plugin\migrate\process;

use Drupal\migrate\Plugin\migrate\process\Extract as ExtractBase;

/**
 * This plugin extracts a value from an array, but does not handle multiple values like the core extract.
 *
 * @link https://www.drupal.org/node/2152731 Online handbook documentation for extract process plugin @endlink
 *
 * @MigrateProcessPlugin(
 *   id = "album_d6migrate_extract",
 * )
 */
class Extract extends ExtractBase {
  // This is overriding the handle_multiple property of the base extract plugin
  // which is in the definition so there's nothing to do here.
}
