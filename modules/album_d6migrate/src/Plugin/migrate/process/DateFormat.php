<?php

namespace Drupal\album_d6migrate\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * This plugin changes a date into the given date format.
 *
 * @MigrateProcessPlugin(
 *   id = "date_format"
 * )
 */
class DateFormat extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (empty($this->configuration['date_format'])) {
      throw new MigrateException('You must define a date_format to use the date_format plugin.');
    }

    if (is_string($value)) {
      $value = date($this->configuration['date_format'], strtotime($value));
    }
    else {
      if (!empty($value['value'])) {
        $value['value'] = date($this->configuration['date_format'], strtotime($value['value']));
      }
      // Handle end date values from d6.
      if (!empty($value['value2'])) {
        $value['end_value'] = date($this->configuration['date_format'], strtotime($value['value2']));
        unset($value['value2']);
      }
    }
    return $value;
  }

}
