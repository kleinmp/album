<?php

namespace Drupal\album_d6migrate\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * This plugin allows arithmetic to be performed on an entry.
 *
 * @code
 * field_text:
 *  plugin: arithmetic
 *  source: transform_value
 *  constant: 2
 *  operator: '+'
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "arithmetic"
 * )
 */
class Arithmetic extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (empty($this->configuration['operator']) || empty($this->configuration['constant'])) {
      throw new MigrateException('You must define an operator and a constant to use this plugin.');
    }

    if (is_array($value)) {
      if ($this->validValue($value['value'])) {
        $value['value'] = $this->operate($value['value'], $this->configuration['operator'], $this->configuration['constant']);
      }
    }
    elseif ($this->validValue($value)) {
      $value = $this->operate($value, $this->configuration['operator'], $this->configuration['constant']);
    }
    return $value;
  }

  /**
   * Check if a value is numeric.
   *
   * @param mixed $value
   *   The value to check.
   *
   * @return bool
   *   Whether or not value is valid.
   */
  public function validValue($value) :mixed {
    return isset($value) && (ctype_digit($value) || is_numeric($value));
  }

  /**
   * Apply operator to value.
   *
   * @param mixed $value
   *   The value on which to operate.
   * @param string $operator
   *   The operator.
   * @param mixed $constant
   *   A constant to apply with operator.
   *
   * @return mixed
   *   The value after operation.
   */
  public function operate($value, $operator, $constant) :mixed {
    switch ($operator) {
      case '-':
        return $value - $constant;

      case '+':
        return $value + $constant;

      case '*':
        return $value + $constant;

      case '/':
        return $value / $constant;
    }
    return $value;
  }

}
