<?php

namespace Drupal\album_import\Form;

use Drupal\album_import\MusicBrainzApi;
use Drupal\album_import\AlbumImportApi;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Album search form.
 */
class AlbumImportCreate extends FormBase {

  /**
   * The album import api.
   *
   * @var \Drupal\album_import\AlbumImportApi
   */
  protected AlbumImportApi $albumImportApi;

  /**
   * The musicbrainz api.
   *
   * @var \Drupal\album_import\MusicBrainzApi
   */
  protected MusicBrainzApi $musicBrainzApi;

  /**
   * The currently active request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected Request $request;

  /**
   * Creates a AlbumImportCreate instance.
   *
   * @param \Drupal\album_import\AlbumImportApi $album_import_api
   *   The album import api.
   * @param \Drupal\album_import\MusicBrainzApi $musicbrainz_api
   *   The MusicBrainz api.
   * @param Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(
    AlbumImportApi $album_import_api,
    MusicBrainzApi $musicbrainz_api,
    RequestStack $request_stack,
  ) {
    $this->albumImportApi = $album_import_api;
    $this->musicBrainzApi = $musicbrainz_api;
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('album_import.album_import_api'),
      $container->get('album_import.musicbrainz_api'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'album_import_create';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $queries = $this->request->query->all();
    foreach (['artist', 'album', 'date', 'type'] as $key) {
      if (!isset($queries[$key])) {
        $form['error'] = [
          '#type' => 'markup',
          '#markup' => $this->t('You are missing the %key parameter. <a href="/album-import/search">Go home</a>.', ['%key' => $key]),
        ];
        return $form;
      }
    }
    if (!empty($queries['mbid'])) {
      $options = [
        'entity' => 'release-group',
        'mbid' => $queries['mbid'],
        'inc' => 'releases+artists',
      ];
    }
    else {
      $options = [
        'query' => [
          'artist' => $queries['artist'],
          'title' => $queries['album'],
        ],
      ];
      if ($queries['type'] != 'Any') {
        $options['query']['releasetypes'] = $queries['type'];
      }
    }
    $results = $this->musicBrainzApi->query($options);
    if (!empty($results->releases)) {
      $release_options = [];
      $releases = [];
      foreach ($results->releases as $release) {
        if (!isset($release->score) || $release->score >= $queries['score']) {
          $parsed_release = $this->musicBrainzApi->parseRelease($release);
          $releases[$release->id] = $parsed_release;
          if (!empty($parsed_release['cover'])) {
            $parsed_release['cover'] = '<img src="' . $parsed_release['cover']->thumbnails->small . '" height="100" width="100"></img>';
          }
          $parsed_release['link'] = '<a href="' . $parsed_release['link'] . '" target="_blank">MusicBrainz Release</a>';
          $release_options[$release->id] = $parsed_release;
        }
      }
      $release_options = array_map(function ($value) {
        $list = '<ul>';
        foreach ($value as $name => $option) {
          $option_value = (string) $option;
          $list .= $this->t("<li> $name: $option_value</li>");
        }
        $list . '</ul>';
        return $list;
      }, $release_options);

      $form['picks'] = [
        '#title' => $this->t('Results'),
        '#type' => 'checkboxes',
        '#options' => $release_options,
        '#required' => TRUE,
      ];
      $form['releases'] = [
        '#type' => 'value',
        '#value' => $releases,
      ];
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Submit'),
      ];
    }
    else {
      $form['no_results'] = [
        '#type' => 'markup',
        '#markup' => $this->t('You got nuthin\'. <a href="/album-import/search">Go home</a>.'),
      ];
    }

    $form['date_received'] = [
      '#type' => 'value',
      '#value' => $queries['date'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // @todo any validation needed?
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $picks = array_filter($values['picks']);
    // If there are multiple picks, then we have to merge the tracks
    // and set the track numbers.
    if (!empty($picks)) {
      $offset = 0;
      $release = $values['releases'][reset($picks)];
      $release['tracks'] = [];
      $release['disc_count'] = count($picks);
      foreach ($picks as $pick) {
        $full_release = $this->musicBrainzApi->query([
          'entity' => 'release',
          'mbid' => $pick,
          'inc' => 'artists+recordings+labels',
          'parse' => TRUE,
        ]);
        if (!empty($full_release['tracks'])) {
          array_walk($full_release['tracks'], function (&$track, $key, $offset) {
            // @todo sometimes there's no track_number?
            $track['total_track_number'] += $offset;
          }, $offset);
          $release['tracks'] = array_merge($release['tracks'], $full_release['tracks']);
          $offset += count($full_release['tracks']);
        }
        if (empty($release['artist'])) {
          $release['artist'] = $full_release['artist'];
          $release['artist_id'] = $full_release['artist_id'];
        }
      }
      $node = $this->albumImportApi->createAlbumEntry($release, $values['date_received']);
      if (!empty($node)) {
        $form_state->setRedirect('entity.node.canonical', ['node' => $node->id()]);
      }
    }
  }

}
