<?php
/**
 * @file
 * Contains \Drupal\album_import\Form\AlbumImportSearch.
 */

namespace Drupal\album_import\Form;

use Drupal\album\AlbumApi;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Album search form.
 */
class AlbumImportSearch extends FormBase {

  /**
   * The album api.
   *
   * @var \Drupal\album\AlbumApi
   */
  protected AlbumApi $albumApi;

  /**
   * Creates a BestAlbumAdd instance.
   *
   * @param \Drupal\album\AlbumApi $album_api
   *   The album api.
   */
  public function __construct(AlbumApi $album_api) {
    $this->albumApi = $album_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('album.album_api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'album_import_search';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['artist'] = [
      '#title' => $this->t('Artist'),
      '#type' => 'textfield',
      '#required' => TRUE,
    ];
    $form['album'] = [
      '#title' => $this->t('Album'),
      '#type' => 'textfield',
      '#required' => TRUE,
    ];
    $form['score'] = [
      '#title' => $this->t('Score'),
      '#type' => 'number',
      '#max' => 100,
      '#min' => 1,
      '#required' => TRUE,
      '#default_value' => 90,
    ];
    $form['type'] = [
      '#title' => $this->t('Type of Release'),
      '#type' => 'select',
      '#options' => [
        'Any' => $this->t('Any'),
        'Album' => $this->t('Album'),
        'Single' => $this->t('Single'),
        'EP' => $this->t('EP'),
        'Compilation' => $this->t('Compilation'),
        'Soundtrack' => $this->t('Soundtrack'),
        'Spokenword' => $this->t('Spokenword'),
        'Interview' => $this->t('Interview'),
        'Live' => $this->t('Live'),
        'Remix' => $this->t('Remix'),
        'Other' => $this->t('Other'),
      ],
      '#default_value' => 'Any',
      '#required' => TRUE,
    ];
    $form['mbid'] = [
      '#title' => $this->t('Music Brainz Id'),
      '#description' => $this->t("In case the search isn't working, directly add the release-group id here."),
      '#type' => 'textfield',
      '#default_value' => '',
    ];
    $form['date_received'] = [
      '#title' => $this->t('Date Received'),
      '#type' => 'date',
      '#date_date_format' => 'm-d-Y',
      '#required' => TRUE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($this->albumApi->contentExists($form_state->getValue('artist'), 'artist')) {
      if ($this->albumApi->contentExists($form_state->getValue('album'), 'album')) {
        $form_state->setErrorByName('album', $this->t('The album %album already exists.', ['%album' => $form_state->getValue('album')]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $url = Url::fromRoute('album_import.create', [
      'artist' => $values['artist'],
      'album' => $values['album'],
      'date' => $values['date_received'],
      'type' => $values['type'],
      'score' => $values['score'],
      'mbid' => $values['mbid'],
    ]);
    $form_state->setRedirectUrl($url);
  }

}
