<?php

namespace Drupal\album_import;

use Drupal\album\AlbumApi;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\file\Entity\File;
use Drupal\file\FileRepositoryInterface;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Class for importing albums.
 */
class AlbumImportApi {

  /**
   * The album api.
   *
   * @var \Drupal\album\AlbumApi
   */
  protected AlbumApi $albumApi;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The file repository.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected FileRepositoryInterface $fileRepository;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * AlbumImportApi constructor.
   *
   * @param \Drupal\album\AlbumApi $album_api
   *   The album api.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\file\FileRepositoryInterface $file_repository
   *   The file repository.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger channel.
   */
  public function __construct(
    AlbumApi $album_api,
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    FileRepositoryInterface $file_repository,
    FileSystemInterface $file_system,
    LoggerChannelInterface $logger,
  ) {
    $this->albumApi = $album_api;
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->fileRepository = $file_repository;
    $this->fileSystem = $file_system;
    $this->logger = $logger;
  }

  /**
   * Create an entire album entry (including artist).
   *
   * @param array $release
   *   An array of info used to create artist, albums, and tracks.
   * @param string $date_received
   *   The date this album was received (ISO-Date)
   *
   * @return \Drupal\node\Entity\Node
   *   The album that was created.
   */
  public function createAlbumEntry(array $release, string $date_received) : Node {
    if (empty($release['artist'])) {
      throw new \Exception('No artist name supplied.');
    }
    $artist_nids = $this->albumApi->contentExists($release['artist'], 'artist');
    if (!empty($artist_nids)) {
      $artist = $this->entityTypeManager->getStorage('node')->load(reset($artist_nids));
    }
    else {
      $artist = self::createArtistNode($release['artist_id'], $release['artist']);
    }

    return self::createAlbumNode($release, $artist, $date_received);
  }

  /**
   * Create an artist node.
   *
   * @param string $artist_id
   *   A unique id from the external source.
   * @param string $name
   *   The human readable name of the artist.
   *
   * @return Drupal\node\Entity\Node
   *   The artist that was created.
   */
  public function createArtistNode(string $artist_id, string $name) : Node {
    $node = $this->entityTypeManager->getStorage('node')->create([
      'type' => 'artist',
      'title' => $name,
      'uid' => '1',
      'status' => 1,
    ]);
    $node->set('field_artist_id', $artist_id);
    $node->save();
    return $node;
  }

  /**
   * Create an album node.
   *
   * @param array $release
   *   An array of info used to create albums, and tracks.
   * @param \Drupal\node\Entity\Node $artist
   *   The artist to be associated with the album.
   * @param string $date_received
   *   The date this album was received (ISO-Date).
   *
   * @return \Drupal\node\Entity\Node
   *   The album that was created.
   */
  public function createAlbumNode(array $release, Node $artist, string $date_received) : Node {
    if (!empty($release['cover'])) {
      $file = self::createLocalFile($release['cover']->image);
    }

    $node = $this->entityTypeManager->getStorage('node')->create([
      'type' => 'album',
      'title' => $release['title'],
      'uid' => '1',
      'status' => 1,
    ]);
    $node->set('field_album_id', $release['id']);
    $node->set('field_received', $date_received);
    $node->set('field_disc_count', $release['disc_count']);
    $node->set('field_artist_to_album', [
      'target_id' => $artist->id(),
    ]);
    $node->set('field_year', substr($release['date'], 0, 4));
    if (!empty($file)) {
      $node->set('field_album_cover', [
        'target_id' => $file->id(),
      ]);
    }
    $field_tracks = [];
    foreach ($release['tracks'] as $track) {
      $paragraph = self::createTrackParagraph($track);
      $field_tracks[] = [
        'target_id' => $paragraph->id(),
        'target_revision_id' => $paragraph->getRevisionId(),
      ];
    }
    $node->set('field_tracks', $field_tracks);

    $node->save();
    return $node;
  }

  /**
   * Create a track paragraph.
   *
   * @param array $values
   *   An array of info used to a track.
   *
   * @return \Drupal\paragraphs\Entity\Paragraph
   *   The track that was created.
   */
  public function createTrackParagraph(array $values) : Paragraph {
    $values += [
      'title' => '',
      'id' => '',
      'length' => 0,
      'track_number' => 0,
    ];
    $paragraph = $this->entityTypeManager->getStorage('paragraph')->create([
      'type' => 'track',
      'field_title' => $values['title'],
      'field_track_number' => $values['total_track_number'],
      'field_length' => $values['length'],
      'field_musicbrainz_id' => $values['id'],
    ]);
    $paragraph->save();
    return $paragraph;
  }

  /**
   * Create a local file from a remote file.
   *
   * @param string $url
   *   The external url of the file to pull in.
   *
   * @return \Drupal\file\Entity\File
   *   The new file.
   */
  public function createLocalFile(string $url) : File {
    $default_schema = $this->configFactory->get('system.file')->get('default_scheme');
    $destination = $default_schema . '://albums/' . $this->fileSystem->basename($url);
    return $this->fileRepository->writeData(file_get_contents($url), $destination);
  }

}
