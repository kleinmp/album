<?php

namespace Drupal\album_import;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Url;
use GuzzleHttp\ClientInterface;

/**
 * MusicBrainz integration client.
 */
class MusicBrainzApi {

  const URL = 'https://musicbrainz.org/ws/2/';
  const COVER_URL = 'https://coverartarchive.org/release/';

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * MusicBrainzApi constructor.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   A Guzzle client object.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger channel.
   */
  public function __construct(
    ClientInterface $http_client,
    ConfigFactoryInterface $config_factory,
    LoggerChannelInterface $logger,
  ) {
    $this->httpClient = $http_client;
    $this->configFactory = $config_factory;
    $this->logger = $logger;
  }

  /**
   * Query the MusicBrainz database.
   *
   * @param array $options
   *   A series of options to limit and control the query. (See below)
   *
   * @return array|object|string
   *   The parsed json array or raw json string depending on the options.
   */
  public function query(array $options) : array|object|string {
    $options += [
      'entity' => 'release',
      'query' => [],
      'mbid' => NULL,
      'fmt' => 'json',
      'inc' => NULL,
      'limit' => NULL,
      'offset' => NULL,
      'release' => NULL,
      'parse' => FALSE,
    ];
    $request_options = [
      'query' => [
        'fmt' => $options['fmt'],
      ],
      'headers' => [
        'User-Agent' => $this->configFactory->get('album_import.settings')->get('user_agent'),
      ],
    ];

    $url = self::URL . $options['entity'];

    if (!empty($options['query'])) {
      array_walk($options['query'], function (&$value, $key) {
        $value = $key . ':' . $value;
      });
      $request_options['query']['query'] = implode(' AND ', $options['query']);
    }
    if (!empty($options['mbid'])) {
      $url .= '/' . $options['mbid'];
    }
    foreach (['inc', 'limit', 'offset', 'release'] as $key) {
      if (!empty($options[$key])) {
        $request_options['query'][$key] = $options[$key];
      }
    }

    $request = $this->httpClient->request('GET', $url, $request_options);

    if ($request->getStatusCode() != 200) {
      throw new \Exception('Request to musicbrainz failed with status ' . $request->getStatusCode());
    }
    $json = json_decode($request->getBody());
    return $options['parse'] ? self::parse($options['entity'], $json) : $json;
  }

  /**
   * Parse the returned json string.
   *
   * @param string $entity
   *   The type of entity requested from music brainz.
   * @param object $json
   *   The decoded json string returned.
   *
   * @return array
   *   A parsed version of the returned json.
   */
  public function parse(string $entity, object $json) : array {
    switch ($entity) {
      case 'release':
        return self::parseRelease($json);
    }
    return $json;
  }

  /**
   * Parse the returned release object.
   *
   * @param object $release
   *   The type of entity requested from music brainz.
   *
   * @return array
   *   A parsed version of the returned json.
   */
  public function parseRelease(object $release) : array {
    $parsed = [
      'id' => $release->id,
      'title' => $release->title,
      'link' => 'https://www.musicbrainz.org/release/' . $release->id,
    ];
    foreach (['country', 'date', 'track-count', 'score'] as $key) {
      if (!empty($release->{$key})) {
        $parsed[$key] = $release->{$key};
      }
    }
    if (!empty($release->{'artist-credit'})) {
      $parsed['artist'] = reset($release->{'artist-credit'})->artist->name;
      $parsed['artist_id'] = reset($release->{'artist-credit'})->artist->id;
    }
    if (!empty($release->{'label-info'})) {
      $parsed['label'] = reset($release->{'label-info'})->label->name;
    }
    if (!empty($release->{'media'})) {
      $tracks = [];
      $total_tracks = 0;
      foreach ($release->{'media'} as $disc) {
        if (empty($parsed['format']) && !empty($disc->format)) {
          $parsed['format'] = $disc->format;
        }
        if (!empty($disc->tracks)) {
          foreach ($disc->tracks as $track) {
            $track->disc = $disc->position;
            $tracks[$track->id] = self::parseTrack($track, $total_tracks);
          }
          $total_tracks += count($disc->tracks);
        }
      }
      if (!empty($tracks)) {
        uasort($tracks, function ($a, $b) {
          return $a['total_track_number'] <=> $b['total_track_number'];
        });
        $parsed['tracks'] = $tracks;
      }
    }
    try {
      if ($cover_data = self::findCover($release->id)) {
        $parsed['cover'] = $cover_data;
      }
    }
    catch (\Exception $e) {
      // It probably just wasn't found.
      // No need to worry about it.
    }
    return $parsed;
  }

  /**
   * Parse a track from a musicbrainz release.
   *
   * @param object $track
   *   Track from within a musicbrainz release.
   * @param int $previous_disc_track_count
   *   The count of tracks from all previously counted discs.
   *
   * @return array
   *   An array of track data.
   */
  public function parseTrack(object $track, int $previous_disc_track_count = 0) : array {
    $parsed = [
      'id' => $track->id,
      'title' => $track->title,
      'track_number' => $track->number,
      'total_track_number' => (int) $track->number + $previous_disc_track_count,
      'length' => $track->length,
      'disc' => $track->disc,
    ];
    return $parsed;
  }

  /**
   * Find the album cover if it exists.
   *
   * @param string $album_id
   *   MusicBrainz release id.
   *
   * @return object|null
   *   Encoded image data of the album cover if it exists.
   */
  public function findCover(string $album_id) : ?object {
    $cover_data = NULL;
    $url = self::COVER_URL . $album_id;
    $request = $this->httpClient->request('GET', $url);
    if ($request->getStatusCode() != 200) {
      throw new \Exception('Request to coverartarchive.org failed with status ' . $request->getStatusCode());
    }
    $data = json_decode($request->getBody());
    if (!empty($data->images)) {
      $cover_data = reset($data->images);
    }
    return $cover_data;
  }

}
