# Album Module Suite

Drupal module suite for managing a collection of music records.

## Installation

Install a you would any other Drupal module.

## Modules

- Album: The main module which provides the data structure for album and artist nodes as well as pages for display.
- Import: Provides a way to import album and artist info from the [Musicbrainz](https://www.musicbrainz.org) wiki.
- Snapshot: Provides the snapshot content type to take snapshots in time of the best album data.
- List: Provides custom code related to the list content type for creating lists of albums an artists.
- D6 Migrate: Provides code for migrating data from a specific Drupal 6 site. It's just here for posterity.
